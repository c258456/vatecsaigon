using System.Collections.Generic;
using App.Core.Utilities;
using App.Domain.ContactInformations;
using App.Domain.Interfaces.Repository;

namespace App.Infra.Data.Repository.ContactInformations
{
    public interface IContactInformationRepository : IRepositoryBase<ContactInformation>
	{
        ContactInformation GetById(int id);

		IEnumerable<ContactInformation> PagedList(Paging page);

		IEnumerable<ContactInformation> PagedSearchList(SortingPagingBuilder sortBuider, Paging page);
	}
}