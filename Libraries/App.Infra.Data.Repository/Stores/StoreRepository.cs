using App.Core.Utilities;
using App.Domain.Stores;
using App.Infra.Data.Common;
using App.Infra.Data.DbFactory;
using System.Collections.Generic;
using System.Linq;

namespace App.Infra.Data.Repository.Stores
{
    public class StoreRepository : RepositoryBase<Store>, IStoreRepository
    {
        public StoreRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Store GetById(int id)
        {
            var Store = FindBy(x => x.Id == id).FirstOrDefault();

            return Store;
        }

        protected override IOrderedQueryable<Store> GetDefaultOrder(IQueryable<Store> query)
        {
            var Store =
                from p in query
                orderby p.Id
                select p;
            return Store;
        }

        public IEnumerable<Store> PagedList(Paging page)
        {
            return GetAllPagedList(page).ToList();
        }

        public IEnumerable<Store> PagedSearchList(SortingPagingBuilder sortBuider, Paging page)
        {
            var expression = PredicateBuilder.True<Store>();

            if (!string.IsNullOrEmpty(sortBuider.Keywords))
            {
                expression = expression.And(x => x.Name.ToLower().Contains(sortBuider.Keywords.ToLower()));
            }

            return FindAndSort(expression, sortBuider.Sorts, page);

        }
    }
}