﻿using App.Core.Utilities;
using App.Domain.Interfaces.Repository;
using App.Domain.Stores;
using System.Collections.Generic;

namespace App.Infra.Data.Repository.Stores
{
    public interface IStoreRepository : IRepositoryBase<Store>
    {
        Store GetById(int id);

        IEnumerable<Store> PagedList(Paging page);

        IEnumerable<Store> PagedSearchList(SortingPagingBuilder sortBuider, Paging page);
    }
}
