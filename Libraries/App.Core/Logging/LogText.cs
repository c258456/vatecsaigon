﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace App.Core.Logging
{
    public static class LogText
    {
        public static bool Log(string txt)
        {
            bool flag;
            try
            {
                HttpContext current = HttpContext.Current;
                string str = "";
                if (current != null)
                {
                    str = current.Request.Url.ToString();
                }
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Logs")))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Logs"));
                }

                DateTime now = DateTime.Now;
                string fileName = $"Logs/{now:yyyyMMdd}.txt";
                string path = string.Concat(current.Server.MapPath("~/"), fileName);

                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                }

                List<string> writeContent = new List<string>
                    {
                        $"{DateTime.Now}\t{txt}\t{str}\r\n"
                    };

                File.AppendAllText(path, string.Join(Environment.NewLine, writeContent));

                //using (var fileStream = File.AppendAllText(path, string.Join(Environment.NewLine, writeContent)))
                //{
                //    //FileStream fileStream = File.Open(string.Concat(current.Server.MapPath("~/"), path), FileMode.Open);
                //    //StreamWriter sddtr = File.AppendText(path);
                //    //File.ReadAllLines(path);


                //    File.AppendAllText(path, string.Join(Environment.NewLine, writeContent));
                //    fileStream.Close();

                //    flag = true;
                //}

                flag = true;

                return flag;
            }
            catch (Exception ex)
            {
            }

            flag = false;

            return flag;
        }
    }
}
