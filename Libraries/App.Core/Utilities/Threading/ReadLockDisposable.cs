﻿using System;
using System.Threading;

namespace App.Core.Utilities.Threading
{
    public sealed class ReadLockDisposable : IDisposable
    {
        private readonly ReaderWriterLockSlim _rwLock;

        public ReadLockDisposable(ReaderWriterLockSlim rwLock)
        {
            _rwLock = rwLock;
        }

        void IDisposable.Dispose()
        {
            _rwLock.ExitReadLock();
        }
    }
}
