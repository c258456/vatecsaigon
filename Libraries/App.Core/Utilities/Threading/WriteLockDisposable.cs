﻿using System;
using System.Threading;

namespace App.Core.Utilities.Threading
{
	public sealed class WriteLockDisposable : IDisposable
	{
		// Fields
		private readonly ReaderWriterLockSlim _rwLock;

		// Methods
		public WriteLockDisposable(ReaderWriterLockSlim rwLock)
		{
			_rwLock = rwLock;
		}

		void IDisposable.Dispose()
		{
			_rwLock.ExitWriteLock();
		}
	}
}
