﻿using System;
using System.Threading;

namespace App.Core.Utilities.Threading
{
    public sealed class UpgradeableReadLockDisposable : IDisposable
    {
        private readonly ReaderWriterLockSlim _rwLock;

        public UpgradeableReadLockDisposable(ReaderWriterLockSlim rwLock)
        {
            _rwLock = rwLock;
        }

        void IDisposable.Dispose()
        {
            _rwLock.ExitUpgradeableReadLock();
        }

    }
}
