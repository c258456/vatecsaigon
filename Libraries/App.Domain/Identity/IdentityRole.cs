using Microsoft.AspNet.Identity;
using System;

namespace App.Domain.Entities.Identity
{
    public class IdentityRole : IRole<Guid>
    {
        public string Description
        {
            get;
            set;
        }

        public Guid Id { get; set; }

        public string Name
        {
            get;
            set;
        }

        public IdentityRole()
        {
            Id = Guid.NewGuid();
        }

        public IdentityRole(string name) : this()
        {
            Name = name;
        }

        public IdentityRole(string name, string description, Guid id)
        {
            Name = name;
            Description = description;
            Id = id;
        }

        public bool Selected { get; set; }
    }
}