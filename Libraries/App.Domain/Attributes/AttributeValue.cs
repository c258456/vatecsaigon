using App.Core.Common;
using App.Domain.Galleries;
using App.Domain.Posts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Entities.Attribute
{
    public class AttributeValue : AuditableEntity<int>
	{
		[ForeignKey("AttributeId")]
		public virtual Attribute Attribute
		{
			get;
			set;
		}

		public int AttributeId
		{
			get;
			set;
		}

		public string ColorHex
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public virtual ICollection<GalleryImage> GalleryImages
		{
			get;
			set;
		}

		public int? OrderDisplay
		{
			get;
			set;
		}

	    private ICollection<Post> _posts;
        public ICollection<Post> Posts
		{
			get => _posts ?? (_posts = new HashSet<Post>());
		    set => _posts = value;
		}

		public int Status
		{
			get;
			set;
		}

		public string ValueName
		{
			get;
			set;
		}

		public AttributeValue()
		{
		}
	}
}