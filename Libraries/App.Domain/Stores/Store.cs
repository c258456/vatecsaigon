﻿using App.Core.Common;
using App.Domain.Account;
using App.Domain.Posts;
using System;
using System.Collections.Generic;

namespace App.Domain.Stores
{
    public class Store : AuditableEntity<int>
    {
        public string Name
        {
            get; set;
        }

        public string ImageUrl
        {
            get;
            set;
        }

        public string Url { get; set; }
        public string Slogan { get; set; }
        public string ShortDesc { get; set; }
        public int Status { get; set; }

        public virtual Guid UserId
        {
            get; set;
        }

        public virtual User User
        {
            get;
            set;
        }

        private ICollection<Post> _posts;
        public virtual ICollection<Post> Posts
        {
            get => _posts ?? (_posts = new HashSet<Post>());
            set => _posts = value;
        }
    }
}
