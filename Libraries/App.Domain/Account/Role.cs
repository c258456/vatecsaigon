using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using App.Core.Common;

namespace App.Domain.Account
{
    public class Role : Entity<Guid>
    {
        private ICollection<User> _users;

        [StringLength(450)]
        public string Description
        {
            get;
            set;
        }

        [StringLength(250)]
        public string Name
        {
            get;
            set;
        }

        public ICollection<User> Users
        {
            get => _users ?? (_users = new HashSet<User>());
            set => _users = value;
        }
    }
}