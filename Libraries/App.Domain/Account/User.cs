using System;
using System.Collections.Generic;
using App.Core.Common;
using App.Domain.Stores;

namespace App.Domain.Account
{
    public class User : Entity<Guid>
    {
        private ICollection<Claim> _claims;

        private ICollection<ExternalLogin> _externalLogins;

        private ICollection<Role> _roles;

        public string Address
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public virtual ICollection<Claim> Claims
        {
            get => _claims ?? (_claims = new HashSet<Claim>());
            set => _claims = value;
        }

        public DateTime Created
        {
            get;
            set;
        }

        public string DisplayAddress
        {
            get
            {
                string addr = string.IsNullOrEmpty(Address) ? string.Empty : Address;
                string city = string.IsNullOrEmpty(City) ? string.Empty : City;

                return $"{addr} {city} {(string.IsNullOrEmpty(State) ? string.Empty : State)}";
            }
        }

        public string DisplayName
        {
            get
            {
                string fName = string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName;
                string mName = string.IsNullOrEmpty(MiddleName) ? string.Empty : MiddleName;

                return $"{(string.IsNullOrEmpty(LastName) ? string.Empty : LastName)} {mName} {fName}";
            }
        }

        public string Email
        {
            get;
            set;
        }

        public bool EmailConfirmed
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public bool IsLockedOut
        {
            get;
            set;
        }

        public bool IsSuperAdmin
        {
            get;
            set;
        }

        public DateTime? LastLogin
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public virtual ICollection<ExternalLogin> Logins
        {
            get => _externalLogins ?? (_externalLogins= new HashSet<ExternalLogin>());
            set => _externalLogins = value;
        }

        public string MiddleName
        {
            get;
            set;
        }

        public virtual string PasswordHash
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public virtual ICollection<Role> Roles
        {
            get => _roles ?? (_roles= new HashSet<Role>());
            set => _roles = value;
        }

        public virtual string SecurityStamp
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        //public ICollection<Store> Stores
        //{
        //    get;
        //    set;
        //}

        private ICollection<Store> _stores;
        public virtual ICollection<Store> Stores
        {
            get => _stores ?? (_stores = new HashSet<Store>());
            set => _stores = value;
        }
    }
}