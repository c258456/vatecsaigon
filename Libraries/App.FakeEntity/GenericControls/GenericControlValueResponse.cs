﻿using System.Collections.Generic;

namespace App.FakeEntity.GenericControls
{
    public class GenericControlValueResponse
    {
        public string __RequestVerificationToken { get; set; }
        public List<ControlValueItemResponse> controlValueItemResponse { get; set; }
    }

    public class ControlValueItemResponse
    {        
        public int GenericControlValueId { get; set; }

        public string Name { get; set; }
        public string ValueName { get; set; }

    }
}