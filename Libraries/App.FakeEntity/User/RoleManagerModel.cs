using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace App.FakeEntity.User
{
    public class RoleManagerModel
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }


    }
}