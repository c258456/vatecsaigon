﻿using App.Domain.Posts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Resources;

namespace App.FakeEntity.Stores
{
    public class StoreViewModel
    {
        public int Id
        {
            get;
            set;
        }

        [DisplayName("Tên shop")]
        [Required(ErrorMessage = "Vui lòng nhập tên shop.")]
        public string Name
        {
            get; set;
        }

        [Display(Name = "ImageUrl", ResourceType = typeof(FormUI))]
        public HttpPostedFileBase ImageFile
        {
            get;
            set;
        }

        public string ImageUrl
        {
            get;
            set;
        }

        public string Url { get; set; }
        public string Slogan { get; set; }
        public string ShortDesc { get; set; }
        public int Status { get; set; }

        public virtual Guid UserId
        {
            get; set;
        }

        private Domain.Account.User _user;

        public virtual Domain.Account.User User
        {
            get => _user;
            set
            {
                _user = value ?? throw new ArgumentNullException("value");
                UserId = value.Id;
            }
        }

        private ICollection<Post> _posts;
        public virtual ICollection<Post> Posts
        {
            get => _posts ?? (_posts = new HashSet<Post>());
            set => _posts = value;
        }
    }
}
