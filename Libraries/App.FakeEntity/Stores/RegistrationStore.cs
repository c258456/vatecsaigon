﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.FakeEntity.Stores
{
    public class RegistrationStore
    {
        public SalerViewModel Saler { get; set; }

        public StoreViewModel Store { get; set; }
    }
}
