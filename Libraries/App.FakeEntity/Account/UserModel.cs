using App.Core.Common;
using System;

namespace App.FakeEntity.Account
{
    public class UserModel : Entity<Guid>
	{
		public string Address
		{
			get;
			set;
		}

		public string City
		{
			get;
			set;
		}

		public DateTime Created
		{
			get;
			set;
		}

		public string DisplayAddress
		{
			get
			{
				string addr = string.IsNullOrEmpty(Address) ? string.Empty : Address;
				string city = string.IsNullOrEmpty(City) ? string.Empty : City;

				return $"{addr} {city} {(string.IsNullOrEmpty(State) ? string.Empty : State)}";
			}
		}

		public string DisplayName
		{
			get
			{
				string fName = string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName;
				string mName = string.IsNullOrEmpty(MiddleName) ? string.Empty : MiddleName;

				return $"{(string.IsNullOrEmpty(LastName) ? string.Empty : LastName)} {mName} {fName}";
			}
		}

		public string Email
		{
			get;
			set;
		}

	    public bool EmailConfirmed
        {
	        get;
	        set;
	    }

        public string FirstName
		{
			get;
			set;
		}

		public bool IsLockedOut
		{
			get;
			set;
		}

		public bool IsSuperAdmin
		{
			get;
			set;
		}

		public DateTime? LastLogin
		{
			get;
			set;
		}

		public string LastName
		{
			get;
			set;
		}

		public string MiddleName
		{
			get;
			set;
		}

		public virtual string PasswordHash
		{
			get;
			set;
		}

		public string Phone
		{
			get;
			set;
		}

		public virtual string SecurityStamp
		{
			get;
			set;
		}

		public string State
		{
			get;
			set;
		}
        
        public string UserName
		{
			get;
			set;
		}
	}
}