﻿using System.Web.Optimization;

namespace App.Framework.Bundling
{
    public interface IBundlePublisher
    {
        void RegisterBundles(BundleCollection bundles);
    }

   
}
