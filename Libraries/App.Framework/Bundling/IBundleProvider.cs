﻿using System.Web.Optimization;

namespace App.Framework.Bundling
{
    public interface IBundleProvider
    {
        void RegisterBundles(BundleCollection bundles);

        int Priority { get; }
    }
}