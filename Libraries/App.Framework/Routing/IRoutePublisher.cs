﻿using System.Web.Routing;

namespace App.Framework.Routing
{
    public interface IRoutePublisher
    {
        void RegisterRoutes(RouteCollection routeCollection);
    }
}
