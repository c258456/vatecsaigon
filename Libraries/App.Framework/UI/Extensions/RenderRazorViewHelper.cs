using App.Core.Logging;
using System;
using System.IO;
using System.Web.Mvc;

namespace App.Framework.UI.Extensions
{
    public static class RenderRazorViewHelper
    {
        public static string RenderRazorViewToString(this Controller controller, string viewName, object model)
        {
            try
            {
                controller.ViewData.Model = model;

                using (var stringWriter = new StringWriter())
                {
                    var viewEngineResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    var viewContext = new ViewContext(controller.ControllerContext, viewEngineResult.View, controller.ViewData, controller.TempData, stringWriter);

                    viewEngineResult.View.Render(viewContext, stringWriter);
                    viewEngineResult.ViewEngine.ReleaseView(controller.ControllerContext, viewEngineResult.View);

                    return stringWriter.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                LogText.Log(ex.Message);
                return null;
            }
        }
    }
}