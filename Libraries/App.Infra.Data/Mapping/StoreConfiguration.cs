using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using App.Domain.Addresses;
using App.Domain.Stores;

namespace App.Infra.Data.Mapping
{
    public class StoreConfiguration : EntityTypeConfiguration<Store>
	{
		public StoreConfiguration()
		{
			ToTable("Store");

			HasKey(x => x.Id).Property(x => x.Id).HasColumnName("Id").HasColumnType("int")
			    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();

        }
	}
}