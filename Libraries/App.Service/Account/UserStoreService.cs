using App.Domain.Account;
using App.Domain.Entities.Identity;
using App.Infra.Data.Repository.Account;
using App.Infra.Data.UOW.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Claim = System.Security.Claims.Claim;

namespace App.Service.Account
{
    public class UserStoreService : IUserLoginStore<IdentityUser, Guid>,
        IUserClaimStore<IdentityUser, Guid>,
        IUserRoleStore<IdentityUser, Guid>,
        IUserPasswordStore<IdentityUser, Guid>,
        IUserSecurityStampStore<IdentityUser, Guid>,
        IUserEmailStore<IdentityUser, Guid>,
        IUserTokenProvider<IdentityUser, Guid>,
        IIdentityMessageService,
        IUserLockoutStore<IdentityUser, Guid>,
        IUserStoreService

    {
        private readonly IExternalLoginRepository _externalLoginRepository;

        private readonly IRoleRepository _roleRepository;

        private readonly IUnitOfWorkAsync _unitOfWork;

        private readonly IUserRepository _userRepository;

        public UserStoreService(IUnitOfWorkAsync unitOfWork, IUserRepository userRepository,
            IExternalLoginRepository externalLoginRepository, IRoleRepository roleRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _externalLoginRepository = externalLoginRepository;
            _roleRepository = roleRepository;
        }

        public Task AddClaimAsync(IdentityUser user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            var existingUser = _userRepository.FindById(user.Id);
            if (existingUser == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }
            var customClaim = new Domain.Account.Claim
            {
                ClaimType = claim.Type,
                ClaimValue = claim.Value,
                User = existingUser
            };

            existingUser.Claims.Add(customClaim);
            _userRepository.Update(existingUser);

            return _unitOfWork.CommitAsync();
        }

        public Task AddLoginAsync(IdentityUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            var existingUser = _userRepository.FindById(user.Id);
            if (existingUser == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }
            var externalLogin = new ExternalLogin
            {
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey,
                User = existingUser
            };
            existingUser.Logins.Add(externalLogin);
            _userRepository.Update(existingUser);

            return _unitOfWork.CommitAsync();
        }

        public Task AddToRoleAsync(IdentityUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("Argument cannot be null, empty, or whitespace: roleName.");
            }
            var exsitingUser = _userRepository.FindById(user.Id);
            if (exsitingUser == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }
            var role = _roleRepository.FindByName(roleName);
            if (role == null)
            {
                throw new ArgumentException("roleName does not correspond to a Role entity.", "roleName");
            }

            exsitingUser.Roles.Add(role);
            _userRepository.Update(exsitingUser);

            return _unitOfWork.CommitAsync();
        }

        public Task CreateAsync(IdentityUser ideUser)
        {
            var user = ideUser.ToUser(new User());

            _userRepository.Add(user);

            return _unitOfWork.CommitAsync();
        }

        public Task DeleteAsync(IdentityUser user)
        {
            var existingUser = _userRepository.FindById(user.Id);
            _userRepository.Delete(existingUser);

            return _unitOfWork.CommitAsync();
        }

        public void Dispose()
        {
        }

        public Task<IdentityUser> FindAsync(UserLoginInfo login)
        {
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            //IdentityUser identityUser = null;
            var byProviderAndKey = _externalLoginRepository.GetByProviderAndKey(login.LoginProvider, login.ProviderKey);
            if (byProviderAndKey == null)
            {
                return null;
            }

            var ideUser = CastToIdentityUser(byProviderAndKey.User);

            return Task.FromResult(ideUser);

        }

        public Task<IdentityUser> FindByIdAsync(Guid userId)
        {
            var user = _userRepository.FindById(userId);
            var ideUser = CastToIdentityUser(user);

            return Task.FromResult(ideUser);
        }

        public Task<IdentityUser> FindByNameAsync(string userName)
        {
            var user = _userRepository.FindByUserName(userName);

            var ideUser = CastToIdentityUser(user);

            return Task.FromResult(ideUser);
        }

        public Task<IList<Claim>> GetClaimsAsync(IdentityUser ideUser)
        {
            var user = _userRepository.FindById(ideUser.Id);
            if (user == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            var task = Task.FromResult<IList<Claim>>((
                from x in user.Claims
                select new Claim(x.ClaimType, x.ClaimValue)).ToList());

            return task;
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(IdentityUser ideUser)
        {
            if (ideUser == null)
            {
                throw new ArgumentNullException("user");
            }

            var user = _userRepository.FindById(ideUser.Id);
            if (user == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            var task = Task.FromResult<IList<UserLoginInfo>>((
                from x in user.Logins
                select new UserLoginInfo(x.LoginProvider, x.ProviderKey)).ToList());

            return task;
        }

        public Task<string> GetPasswordHashAsync(IdentityUser ideUser)
        {
            return Task.FromResult(ideUser.PasswordHash);
        }

        public Task<IList<string>> GetRolesAsync(IdentityUser ideUser)
        {
            if (ideUser == null)
            {
                throw new ArgumentNullException("user");
            }

            var user = _userRepository.FindById(ideUser.Id);
            if (user == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            var task = Task.FromResult<IList<string>>((
                from x in user.Roles
                select x.Name).ToList());

            return task;
        }

        public Task<string> GetSecurityStampAsync(IdentityUser ideUser)
        {
            return Task.FromResult(ideUser.SecurityStamp);
        }

        public Task<bool> HasPasswordAsync(IdentityUser ideUser)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(ideUser.PasswordHash));
        }

        public Task<bool> IsInRoleAsync(IdentityUser ideUser, string roleName)
        {
            if (ideUser == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("Argument cannot be null, empty, or whitespace: role.");
            }

            var user = _userRepository.FindById(ideUser.Id);
            return user == null
                ? throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user")
                : Task.FromResult(user.Roles.Any(x => x.Name == roleName));
        }

        private IdentityUser CastToIdentityUser(User user)
        {
            if (user == null)
            {
                return null;
            }

            var identityUser = new IdentityUser
            {
                Id = user.Id,
                UserName = user.UserName,
                PasswordHash = user.PasswordHash,
                SecurityStamp = user.SecurityStamp,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                State = user.State,
                City = user.City,
                Phone = user.Phone,
                Address = user.Address,
                IsSuperAdmin = user.IsSuperAdmin,
                IsLockedOut = user.IsLockedOut,
                Created = user.Created,
                Stores = user.Stores
            };

            return identityUser;
        }


        public Task RemoveClaimAsync(IdentityUser ideUser, Claim claim)
        {
            if (ideUser == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

            var existingUser = _userRepository.FindById(ideUser.Id);
            if (existingUser == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            var existingClaim = existingUser.Claims.FirstOrDefault(x => (x.ClaimType == claim.Type && x.ClaimValue == claim.Value));

            existingUser.Claims.Remove(existingClaim);
            _userRepository.Update(existingUser);

            return _unitOfWork.CommitAsync();
        }

        public Task RemoveFromRoleAsync(IdentityUser ideUser, string roleName)
        {
            if (ideUser == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("Argument cannot be null, empty, or whitespace: role.");
            }

            var existingUser = _userRepository.FindById(ideUser.Id);
            if (existingUser == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            var existingRole = existingUser.Roles.FirstOrDefault(x => x.Name == roleName);
            existingUser.Roles.Remove(existingRole);
            _userRepository.Update(existingUser);

            return _unitOfWork.CommitAsync();
        }

        public Task RemoveLoginAsync(IdentityUser ideUser, UserLoginInfo login)
        {
            if (ideUser == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            var existingUser = _userRepository.FindById(ideUser.Id);
            if (existingUser == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            var externalLogin = existingUser.Logins.FirstOrDefault(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey);
            existingUser.Logins.Remove(externalLogin);
            _userRepository.Update(existingUser);

            return _unitOfWork.CommitAsync();
        }

        public Task SetPasswordHashAsync(IdentityUser ideUser, string passwordHash)
        {
            ideUser.PasswordHash = passwordHash;

            return Task.FromResult(0);
        }

        public Task SetSecurityStampAsync(IdentityUser ideUser, string stamp)
        {
            ideUser.SecurityStamp = stamp;

            return Task.FromResult(0);
        }

        public Task UpdateAsync(IdentityUser ideUser)
        {
            if (ideUser == null)
            {
                throw new ArgumentException("user");
            }

            var userExisting = _userRepository.FindById(ideUser.Id);
            if (userExisting == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            var user = ideUser.ToUser(userExisting);

            _userRepository.Update(user);

            return _unitOfWork.CommitAsync();
        }

        public Task<IdentityUser> FindByEmailAsync(string email)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailAsync(IdentityUser user, string email)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetEmailAsync(IdentityUser user)
        {
            return Task.Factory.StartNew(() => user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(IdentityUser user)
        {
            return Task.Factory.StartNew(() => user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(IdentityUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityUser> FindByIdAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task SendAsync(IdentityMessage message)
        {
            SmtpClient client = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("ddemo9698@gmail.com", "abc@12345")
            };
            //client.Timeout = 10000;

            return client.SendMailAsync("ddemo9698@gmail.com", message.Destination, message.Subject, message.Body);
        }

        public Task SendEmailAsync(Guid userId, string subject, string body)
        {
            SmtpClient client = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("ddemo9698@gmail.com", "abc@12345")
            };
            //client.Timeout = 10000;

            return client.SendMailAsync("ddemo9698@gmail.com", "", "", "");
        }

        public Task<string> GenerateAsync(string purpose, UserManager<IdentityUser, Guid> manager, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<IdentityUser, Guid> manager, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task NotifyAsync(string token, UserManager<IdentityUser, Guid> manager, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsValidProviderForUserAsync(UserManager<IdentityUser, Guid> manager, IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(IdentityUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetAccessFailedCountAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetLockoutEnabledAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(IdentityUser user, bool enabled)
        {
            var existingUser = _userRepository.FindById(user.Id);
            if (existingUser == null)
            {
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            }

            existingUser.IsLockedOut = enabled;
            _userRepository.Update(existingUser);

            return _unitOfWork.CommitAsync();
        }
    }

    public static class UserExtension
    {
        public static User ToUser(this IdentityUser identityUser, User user)
        {
            if (identityUser == null)
            {
                return null;
            }

            user.Id = identityUser.Id;
            user.UserName = identityUser.UserName;
            user.PasswordHash = identityUser.PasswordHash;
            user.SecurityStamp = identityUser.SecurityStamp;
            user.Email = identityUser.Email;
            user.EmailConfirmed = identityUser.EmailConfirmed;
            user.FirstName = identityUser.FirstName;
            user.MiddleName = identityUser.MiddleName;
            user.LastName = identityUser.LastName;
            user.State = identityUser.State;
            user.City = identityUser.City;
            user.Phone = identityUser.Phone;
            user.Address = identityUser.Address;
            user.IsSuperAdmin = identityUser.IsSuperAdmin;
            user.IsLockedOut = identityUser.IsLockedOut;
            user.Created = identityUser.Created;

            return user;
        }
    }
}