﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Service.Common
{
    public partial class MobileDeviceHelper : IMobileDeviceHelper
    {
        private readonly IUserAgent _userAgent;

        public MobileDeviceHelper(IUserAgent userAgent)
        {
            _userAgent = userAgent;
        }

        public virtual bool IsMobileDevice()
        {
            return false;
        }
    }
}
