﻿namespace App.Service.Common
{
    public partial interface IMobileDeviceHelper
    {
        /// <summary>
        /// Returns a value indicating whether request is made by a mobile device
        /// </summary>
        /// <returns>Result</returns>
        bool IsMobileDevice();
    }
}
