using App.Core.Utilities;
using App.Domain.Interfaces.Services;
using App.Domain.Stores;
using System.Collections.Generic;

namespace App.Service.Stores
{
    public interface IStoreService : IBaseService<Store>
	{
		Store GetById(int id, bool isCache = true);

		IEnumerable<Store> PagedList(SortingPagingBuilder sortBuider, Paging page);
	}
}