using App.Core.Caching;
using App.Core.Utilities;
using App.Domain.Stores;
using App.Infra.Data.Common;
using App.Infra.Data.Repository.Stores;
using App.Infra.Data.UOW.Interfaces;
using System.Collections.Generic;
using System.Text;

namespace App.Service.Stores
{
    public class StoreService : BaseService<Store>, IStoreService
    {
        private const string CacheKey = "db.Store.{0}";
        private readonly ICacheManager _cacheManager;

        private readonly IStoreRepository _StoreRepository;

        public StoreService(IUnitOfWork unitOfWork, IStoreRepository StoreRepository, ICacheManager cacheManager) : base(unitOfWork, StoreRepository)
        {
            _StoreRepository = StoreRepository;
            _cacheManager = cacheManager;
        }

        public Store GetById(int id, bool isCache = true)
        {
            Store store;

            if (isCache)
            {
                var sbKey = new StringBuilder();
                sbKey.AppendFormat(CacheKey, "GetById");
                sbKey.Append(id);

                var key = sbKey.ToString();
                store = _cacheManager.Get<Store>(key);
                if (store == null)
                {
                    store = _StoreRepository.GetById(id);
                    _cacheManager.Put(key, store);
                }

            }
            else
            {
                store = _StoreRepository.GetById(id);
            }            

            return store;
        }

        public IEnumerable<Store> PagedList(SortingPagingBuilder sortbuBuilder, Paging page)
        {
            return _StoreRepository.PagedSearchList(sortbuBuilder, page);
        }
    }
}