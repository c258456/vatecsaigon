﻿using App.Framework.Routing;
using System.Web.Mvc;
using System.Web.Routing;

namespace App.Front.Infrastructure.Routes
{
    public class StoreRoutes: IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("content/{*pathInfo}");
            routes.IgnoreRoute("images/{*pathInfo}");
            routes.IgnoreRoute("scripts/{*pathInfo}");
            routes.IgnoreRoute("fonts/{*pathInfo}");

            routes.IgnoreRoute("assets/templates/skins/css/fonts/{*pathInfo}");

            //routes.MapRoute("Default", "{controller}/{action}/{id}"
            //  , new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //  , new[] { "App.Front.Controllers" });
        }

        public int Priority => 1;
    }
}