function resizepro(){
	var w=$('.tablecell').width();
	$('.tablecell').css({'height':w});
	$('.tablecell .img-item-pro img').css({'max-height':w});
};
$(window).on('load resize',function() {
	resizepro();
    var w=$('.tablecell').width();
    $('.tablecell').css('height',w);
    $('.tablecell img').css('max-height',w);
    if($(window).width()<740){
        $('.tablecell .img-item-pro').css('height',w);
    }
});
$(document).ready(function(){
    $('span#loc').click(function(){
        var id=$(this).attr('dataid');
        $('#'+id).slideToggle();
    });

	$('#hsx,#ltp,#cs').on('change', function() {
		var parent=$(this).attr('data-parent');
		var hsx=$(".hsx"+parent).val();
		var ltp=$(".ltp"+parent).val();
		var cs=$(".cs"+parent).val();
		$.ajax({
	        url: "/ajax.php",
	        type: 'POST',
	        data: "action=searchpro&hsx="+hsx+"&ltp="+ltp+"&cs="+cs+"&parent="+parent,
		    success: function(data){
		    	$('.wrappro'+parent+' .row').html(data).promise().done(function(){
                    slidepro(parent);
                });
		    }
		});
	});

    function slidepro(id){
        $("#prolist"+id).owlCarousel({
            autoPlay: false,
            items : 3,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            itemsTablet: [768,3],
            itemsTabletSmall: false,
            itemsMobile: [479,2],
            navigation : true,
            navigationText : ["<i class='fa fa-angle-left pro1-l'></i>", "<i class='fa fa-angle-right pro1-r' ></i>"],
            pagination: false
        });
        if(id==529){
            resizepro();
        }
    }
    $('#hsx').on('change', function() {
		var hsx=$(this).val();
		$.ajax({
	        url: "../ajax.php",
	        type: 'POST',
	        data: "action=quotehsx&hsx="+hsx,
		    success: function(data){
		    	$('.quotensx').html(data);
		    }
		});
	});
});