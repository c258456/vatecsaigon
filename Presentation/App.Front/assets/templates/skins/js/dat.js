$(function(){
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function (e) {
        
        $(this).tab('show');
        var scrollmem = $('body').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });
});

$('#xemketqua2').on("click", function() {
    var phone = $('#infophone2').val();
    var infoname = $('#infoname2').val();
    var infoemail = $('#infoemail2').val();
    var infocapchain = $('#infocapchain2').val();
    var capcha1 = $('#capcha2.capcha1').val();
    var dientich = $('#dientich2').val();
    var urls = $('#url').val();
    if (infoname == '') {
        alert(mess.emptyname);
        $('#infoname2').focus();
    } else if (!checknum(dientich)) {
        alert(mess.notnum);
    } else if (!validateEmail(infoemail)) {
        alert(mess.email);
        $('#infoemail2').focus();
    } else if (capcha1 != infocapchain) {
        alert(mess.captcha);
        $('#infocapchain2').focus();
    } else {
        $(this).fadeOut();
        $("#bg-calcu-data2, #bg-calcu-data2 > div#loading2").fadeIn();
        $('#bg-calcu-data2 .wrap-calcu-data').fadeOut();
        var khuvuc = $('#khuvuc2').val();
        var mohinh = $('#mohinh2').val();
        var mohinhtext = $('#mohinh2 option:selected').text();
        var hethongtext = $('#hethong2 option:selected').text();
        $.ajax({
            type: 'POST',
            url: PathMainUrl + '/ajax.php',
            data: "action=dutoanhethong&khuvuc=" + khuvuc + "&dientich=" + dientich + "&mohinh=" + mohinh + "&phone=" + phone + "&name=" + infoname + "&email=" + infoemail + "&url=" + urls + "&mohinhtext=" + mohinhtext + "&hethongtext=" + hethongtext,
            success: function(result) {
                var data = JSON.parse(result);
                $('span#congsuathethong2').html(data['congsuathethong']);
                $('span#soluongtampin2').html(data['soluongtampinsudung']);
                $('span#dientichchiemdung2').html(data['dientichthucte']);
                $('span#gocnghieng2').html(data['gocnghien']);
                $('span#sanluongdukien2').html(data['sanluongdukien']);
                $('span#tietkiem2').html(data['tientietkiem']);
                $('span#co22').html(data['co2']);
                $('#capcha2.capcha1').val(data['capcha']);
                setTimeout(function() {
                    $("#bg-calcu-data2 > div#loading2").fadeOut();
                    setTimeout(function() {
                        $('#bg-calcu-data2 .wrap-calcu-data').fadeIn();
                        $("#xemketqua2").fadeIn();
                    }, 500);
                }, 3000);
            }
        });
    }
});

$('#xemketqua3').on("click", function() {
    var phone = $('#infophone3').val();
    var infoname = $('#infoname3').val();
    var infoemail = $('#infoemail3').val();
    var infocapchain = $('#infocapchain3').val();
    var capcha1 = $('#capcha3.capcha1').val();
    var dientich = $('#dientich3').val();
    var urls = $('#url').val();
    if (infoname == '') {
        alert(mess.emptyname);
        $('#infoname3').focus();
    } else if (!checknum(dientich)) {
        alert(mess.notnum);
    } else if (!validateEmail(infoemail)) {
        alert(mess.email);
        $('#infoemail3').focus();
    } else if (capcha1 != infocapchain) {
        alert(mess.captcha);
        $('#infocapchain3').focus();
    } else {
        $(this).fadeOut();
        $("#bg-calcu-data3, #bg-calcu-data3 > div#loading3").fadeIn();
        $('#bg-calcu-data3 .wrap-calcu-data').fadeOut();
        var khuvuc = $('#khuvuc3').val();
        var mohinh = $('#mohinh3').val();
        var mohinhtext = $('#mohinh3 option:selected').text();
        var hethongtext = $('#hethong3 option:selected').text();
        $.ajax({
            type: 'POST',
            url: PathMainUrl + '/ajax.php',
            data: "action=dutoanhethong&khuvuc=" + khuvuc + "&dientich=" + dientich + "&mohinh=" + mohinh + "&phone=" + phone + "&name=" + infoname + "&email=" + infoemail + "&url=" + urls + "&mohinhtext=" + mohinhtext + "&hethongtext=" + hethongtext,
            success: function(result) {
                var data = JSON.parse(result);
                $('span#congsuathethong3').html(data['congsuathethong']);
                $('span#soluongtampin3').html(data['soluongtampinsudung']);
                $('span#dientichchiemdung3').html(data['dientichthucte']);
                $('span#gocnghieng3').html(data['gocnghien']);
                $('span#sanluongdukien3').html(data['sanluongdukien']);
                $('span#tietkiem3').html(data['tientietkiem']);
                $('span#co23').html(data['co2']);
                $('#capcha3.capcha1').val(data['capcha']);
                setTimeout(function() {
                    $("#bg-calcu-data3 > div#loading3").fadeOut();
                    setTimeout(function() {
                        $('#bg-calcu-data3 .wrap-calcu-data').fadeIn();
                        $("#xemketqua3").fadeIn();
                    }, 500);
                }, 3000);
            }
        });
    }
});
