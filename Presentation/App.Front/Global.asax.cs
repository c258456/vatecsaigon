﻿using App.Core.Infrastructure;
using App.Framework.Bundling;
using App.Framework.FluentValidation;
using App.Framework.Mappings;
using App.Framework.Modelling;
using App.Framework.Routing;
using App.Framework.Theme;
using Autofac;
using Autofac.Builder;
using Autofac.Integration.Mvc;
using FluentValidation.Mvc;
using System;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;

namespace App.Front
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            // we use our own mobile devices support (".Mobile" is reserved). that's why we disable it.
            var mobileDisplayMode = DisplayModeProvider.Instance.Modes.FirstOrDefault(x => x.DisplayModeId == DisplayModeProvider.MobileDisplayModeId);
            if (mobileDisplayMode != null)
            {
                DisplayModeProvider.Instance.Modes.Remove(mobileDisplayMode);
            }

            //bool installed = DataSettings.DatabaseIsInstalled();

            //if (installed)
            //{
            //Remove all view engines
            ViewEngines.Engines.Clear();
            //}

            // Initialize engine context
            var engine = EngineContext.Initialize(false);

            DependencyActive();

            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());

            // Register MVC areas
            AreaRegistration.RegisterAllAreas();

            //Bootstrapper.DependencyActive();

            // Fluent validation
            FluentValidationModelValidatorProvider.Configure(provider => provider.ValidatorFactory = new FluentValidationConfig());

            // Routes
            RegisterRoutes(RouteTable.Routes, engine);

            // localize MVC resources
            ClientDataTypeModelValidatorProvider.ResourceClassKey = "MvcLocalization";
            DefaultModelBinder.ResourceClassKey = "MvcLocalization";

            //if (installed)
            //{
            //register our themeable razor view engine we use
            ViewEngines.Engines.Add(new ThemeableRazorViewEngine());

            //Global filters
            RegisterGlobalFilters();

            // Bundles
            RegisterBundles(BundleTable.Bundles, engine);

            RegisterClassMaps();
            //}
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                //log the error
            }
        }

        private static void RegisterClassMaps()
        {
            // register AutoMapper class maps
            AutoMapperConfiguration.Configure();
        }

        private static void RegisterGlobalFilters()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }

        private static void RegisterRoutes(RouteCollection routes, IEngine engine)
        {
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");
            routes.IgnoreRoute(".db/{*virtualpath}");

            // register routes (core, admin, plugins, etc)
            var routePublisher = engine.Resolve<IRoutePublisher>();
            routePublisher.RegisterRoutes(routes);
        }

        private static void RegisterBundles(BundleCollection bundles, IEngine engine)
        {
            // register custom bundles
            var bundlePublisher = engine.Resolve<IBundlePublisher>();
            bundlePublisher.RegisterBundles(bundles);
        }

        public static ContainerBuilder DependencyActive()
        {
            return ContainerBuilder.Value;
        }

        private static readonly Lazy<ContainerBuilder> ContainerBuilder = new Lazy<ContainerBuilder>(() =>
        {
            var container = new ContainerBuilder();
            SetAutofacContainer(container);

            return container;
        });

        private static void SetAutofacContainer(ContainerBuilder containerBuilder)
        {
           var array = (
                from Assembly p in BuildManager.GetReferencedAssemblies()
                where p.ManifestModule.Name.StartsWith("App.")
                select p).ToArray();

            containerBuilder.RegisterControllers(array);
            containerBuilder.RegisterAssemblyModules(array);
            containerBuilder.RegisterFilterProvider();
            containerBuilder.RegisterSource(new ViewRegistrationSource());

            var container = containerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
