﻿using App.Core.Extensions;
using App.Domain.Entities.Orders;
using App.Domain.Posts;
using App.FakeEntity.Address;
using App.Framework.Filters;
using App.Framework.UI.Extensions;
using App.Front.Models.ShoppingCart;
using App.Service.Common;
using App.Service.Customers;
using App.Service.GenericAttributes;
using App.Service.Orders;
using App.Service.PaymentMethodes;
using App.Service.Posts;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Domain.Customers;

namespace App.Front.Controllers
{
    public class ShoppingCartController : FrontBaseController
    {
        private readonly IShoppingCartItemService _shoppingCartItemService;
        private readonly IPostService _postService;
        private readonly IWorkContext _workContext;
        private readonly IGenericAttributeService _genericAttributeService;

        public ShoppingCartController(IShoppingCartItemService shoppingCartItemService
            , IPostService postService, IWorkContext workContext, IGenericAttributeService genericAttributeService,
            IPaymentMethodService paymentMethodService)
        {
            _shoppingCartItemService = shoppingCartItemService;
            _postService = postService;
            _workContext = workContext;
            _genericAttributeService = genericAttributeService;
        }

        [HttpPost]
        public JsonResult AddProduct(int postId, int quantity, decimal price, FormCollection form)
        {
            var post = _postService.GetById(postId);

            var ctx = new AddToCartContext
            {
                Post = post,
                Quantity = quantity,
                Price = price
            };

            //Create cart
            _shoppingCartItemService.AddToCart(ctx);

            var model = PrepareMiniShoppingCartModel();

          return Json(new { success = true, list = this.RenderRazorViewToString("_Order.TopCart", model) },
                    JsonRequestBehavior.AllowGet);
        }

        private MiniShoppingCartModel PrepareMiniShoppingCartModel()
        {
            var cart = _workContext.CurrentCustomer.GetCartItems();

            var posts = new List<Post>();

            if (cart.IsAny())
            {
                ViewBag.CurrentOrderItem = cart.FirstOrDefault(x => x.CreatedDate >= DateTime.UtcNow.AddSeconds(-2));

                posts.AddRange(cart.Select(item => _postService.GetById(item.PostId, false)));
            }

            var model = new MiniShoppingCartModel
            {
                Items = posts,
                ShoppingCarts = cart,
                SubTotal = _shoppingCartItemService.GetCurrentCartSubTotal(cart)
            };

            return model;
        }

        [HttpPost]
        public JsonResult GetPopupCart()
        {
            var model = PrepareMiniShoppingCartModel();

            return Json(new { success = true, list = this.RenderRazorViewToString("_Order.TopCart", model) },
                    JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteProduct(int id)
        {
            var shppingCart = _shoppingCartItemService.GetById(id);

            _shoppingCartItemService.Delete(shppingCart);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OrderNotification()
        {
            var model = new MiniShoppingCartModel();

            var shoppingCarts = _workContext.CurrentCustomer.ShoppingCartItems;

            if (shoppingCarts.Any())
            {
                var cartItem = shoppingCarts.OrderByDescending(x => x.CreatedDate).First();

                var posts = new List<Post>();
                var existingPost = _postService.GetById(cartItem.PostId);
                posts.Add(existingPost);

                model = new MiniShoppingCartModel
                {
                    Items = posts,
                    ShoppingCarts = shoppingCarts
                };
            }

           return Json(
                    new
                    {
                        success = true,
                        list = this.RenderRazorViewToString("_Order.Notification", model)
                    }, JsonRequestBehavior.AllowGet
                    );
        }

        [NonAction]
        public void PrepareShoppingCartModel(IOrderedEnumerable<ShoppingCartItem> cart)
        {
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }

            if (!cart.Any())
            {
            }
        }

        public ActionResult Cart()
        {
            var cartItems = _workContext.CurrentCustomer.GetCartItems();

            var posts = new List<Post>();

            if (cartItems.Any())
            {
                ViewBag.CurrentOrderItem = cartItems.FirstOrDefault(x => x.CreatedDate >= DateTime.UtcNow.AddSeconds(-2));

                posts.AddRange(cartItems.Select(item => _postService.GetById(item.PostId)));
            }

            var model = new MiniShoppingCartModel
            {
                Items = posts,
                ShoppingCarts = cartItems,
                SubTotal = _shoppingCartItemService.GetCurrentCartSubTotal(cartItems)
            };


            PrepareShoppingCartModel(cartItems);

            return View(model);
        }

        public ActionResult CartItem(MiniShoppingCartModel model)
        {
            return PartialView("_Cart.CartItem", model);
        }

        /// <summary>
        /// Update cart khi thay đổi số lượng
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="quantity"></param>
        /// <param name="price"></param>
        /// <returns>IEnumber tất cả sản phẩm của khách hàng</returns>
        public JsonResult UpdateCartItem(int postId, int quantity, decimal price)
        {
            //Update cart theo số lượng mới
            var post = _postService.GetById(postId);

            var ctx = new AddToCartContext
            {
                Post = post,
                Quantity = quantity,
                Price = price
            };

            //Update cart
            _shoppingCartItemService.AddToCart(ctx);

            var model = PrepareMiniShoppingCartModel();

           return Json(new { success = true, list = this.RenderRazorViewToString("_Cart.CartItem", model) },
                    JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("startcheckout")]
        public ActionResult StartCheckout()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Login", "User");
            }

            var cart = _workContext.CurrentCustomer.GetCartItems();

            return cart.IsAny() ? RedirectToAction("BillingAddress", "Checkout") : RedirectToAction("Index", "Home");
        }

        [ChildActionOnly]
        public ActionResult OrderSummary()
        {
            var cart = _workContext.CurrentCustomer.GetCartItems();
            var model = new ShoppingCartModel();

            PrepareShoppingCartModel(model, cart);

            return PartialView(model);
        }

        private void PrepareShoppingCartModel(ShoppingCartModel model, IOrderedEnumerable<ShoppingCartItem> cart)
        {
            //billing info
            var billAddress = _workContext.CurrentCustomer.Addresses;
            if (billAddress.IsAny())
            {
                foreach (var address in billAddress)
                {
                    var map = Mapper.Map<AddressViewModel>(address);
                    model.OrderReviewData.BillingAddress = map;
                }
            }

            //Shipping info
            var shippingAddress = _workContext.CurrentCustomer.ShippingAddress;
            if (shippingAddress != null)
            {
                var mapShipping = Mapper.Map<AddressViewModel>(shippingAddress);
                model.OrderReviewData.ShippingAddress = mapShipping;
            }

            //Shipping method
            var selectedShippingMethodSystemName = _workContext.CurrentCustomer.GetAttribute("Customer", SystemCustomerAttributeNames.SelectedShippingOption
                , _genericAttributeService);
            model.OrderReviewData.ShippingMethod = selectedShippingMethodSystemName;

            //Payment method
            var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute("Customer", SystemCustomerAttributeNames.SelectedPaymentMethod
                , _genericAttributeService);
            model.OrderReviewData.PaymentMethod = selectedPaymentMethodSystemName;
        }
    }
}