﻿using App.Front.Models;
using App.Service.Authentication.External;
using System.Collections.Generic;
using System.Web.Mvc;

namespace App.Front.Controllers
{
    public class ExternalAuthenticationController : Controller
    {
        private readonly IOpenAuthenticationService _openAuthenticationService;

        public ExternalAuthenticationController(IOpenAuthenticationService openAuthenticationService)
        {
            _openAuthenticationService = openAuthenticationService;
        }

        public ActionResult ExternalMethods()
        {
            var model = new List<ExternalAuthenticationMethodModel>();

            foreach (var eam in _openAuthenticationService.LoadActiveExternalAuthenticationMethods())
            {
                eam.Value.GetPublicInfoRoute(out var actionName, out var controllerName, out var routeValues);

                var eamModel = new ExternalAuthenticationMethodModel
                {
                    ActionName = actionName, ControllerName = controllerName, RouteValues = routeValues
                };

                model.Add(eamModel);
            }

            return PartialView(model);
        }
    }
}