using App.Service.ContactInformations;
using System.Web.Mvc;

namespace App.Front.Controllers
{
    public class GoogleMapController : Controller
	{
		private readonly IContactInformationService _contactInfoService;

		public GoogleMapController(IContactInformationService contactInfoService)
		{
			_contactInfoService = contactInfoService;
		}

		public ActionResult ShowGoogleMap(int id)
		{
			var contactInformation = _contactInfoService.Get(x => x.Id == id);

			return View(contactInformation);
		}
	}
}