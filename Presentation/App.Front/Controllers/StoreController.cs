﻿using App.Core.Extensions;
using App.Domain.Entities.Identity;
using App.Domain.Stores;
using App.FakeEntity.Stores;
using App.FakeEntity.User;
using App.Service.Stores;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace App.Front.Controllers
{
    public class StoreController : BaseAccessUserController
    {
        private readonly IStoreService _storeService;

        public StoreController(UserManager<IdentityUser, Guid> userManager
            , IIdentityMessageService emailService, IStoreService storeService) : base(userManager, emailService)
        {
            _storeService = storeService;
        }

        #region Login

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                var msg = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                    .Select(v => v.ErrorMessage + " " + v.Exception));

                ModelState.AddModelError("", msg);

                return View();
            }

            var user = await UserManager.FindAsync(model.UserName, model.Password);
            if (user == null)
            {
                ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");

                return View();
            }

            //Check store don't register
            if (!user.Stores.IsAny())
            {
                ModelState.AddModelError("", $"Cửa hàng {user.Stores.FirstOrDefault().Name} chưa được đăng ký.");

                return View();
            }

            //Check store approval
            if (user.IsLockedOut)
            {
                ModelState.AddModelError("", $"Admin chưa duyệt cửa hàng {user.Stores.FirstOrDefault().Name}.");

                return View();
            }

            await SignInAsync(user, model.Remember);

            return Redirect("/admin/post");
        }

        private async Task SignInAsync(IdentityUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }

        #endregion

        #region Registration

        [AllowAnonymous]
        public ActionResult RegistrationSaler()
        {
            var model = new RegistrationStore();

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegistrationSaler(RegistrationStore model)
        {
            try
            {
                if (!Request.IsAjaxRequest())
                {
                    var msg = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage + " " + v.Exception));

                    ModelState.AddModelError("", msg);

                    return Json(new
                    {
                        title = "Đăng ký mở shop",
                        success = false,
                        errors = msg
                    });
                }

                var saler = model.Saler;
                var user = new IdentityUser
                {
                    UserName = saler.UserName,
                    Address = saler.Address,
                    FirstName = saler.FirstName,
                    LastName = saler.LastName,
                    MiddleName = saler.MiddleName,
                    Phone = saler.Phone,
                    Email = saler.Email,
                    City = saler.City,
                    State = saler.State,
                    IsLockedOut = true,
                    IsSuperAdmin = false,
                    Created = DateTime.UtcNow
                };

                var ideResult = await UserManager.CreateAsync(user, saler.Password);
                if (ideResult.Succeeded)
                {
                    //Add new store
                    model.Store.Name= $"{user.UserName}-{user.Email}";
                    RegistrationStore(user.Id, model.Store);

                    //Role' saler add to Role management
                    await UserManager.AddToRolesAsync(user.Id, "Saler");

                    return Json(new
                    {
                        title = "Đăng ký bán hàng",
                        success = true,
                        message = "Đăng ký bán hàng, chúng tôi sẽ xét duyệt yêu cầu và liên lạc với bạn ngay khi có thể."
                    });
                }

                var sb = new StringBuilder();
                ideResult.Errors.Each(x => sb.Append(x));

                ModelState.AddModelError("", sb.ToString());

                return Json(new
                {
                    title = "Đăng ký bán hàng không thành công",
                    success = false,
                    errors = sb.ToString()
                });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                throw;
            }
        }

        private void RegistrationStore(Guid userId, StoreViewModel model)
        {
            model.UserId = userId;
            var store = Mapper.Map<StoreViewModel, Store>(model);

            _storeService.Create(store);
        }


        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}