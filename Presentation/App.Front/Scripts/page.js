﻿$(function () {
    window.setLocation = function (url) {
        window.location.href = url;
    };

    $("#startRegistrationStore").click(function () {
        formAjax("#formRegSaler");
        return false;
    });

    $("#btn-send-contact").click(function () {
        var fromId = "#form-send-Contact";
        formAjax(fromId);
        return false;
    });

    $("#CheckProduct").click(function (e) {
        var fromId = "#checking";
        formAjax(fromId);
        return false;
    });
    $("#BuyProduct").click(function (e) {
        var fromId = "#formbuy";
        formAjax(fromId);
        return false;
    });
    $("#checkorder").click(function (e) {
        var code = $("#oderCode").val();
        var name = $("#NameOrPhome").val();
        $.post('/home/CheckOrder'
            , { phone: name, ordercode: code }
            , function (response) {
                $(".result_check").empty().html(response.data);
            }
        );
        return false;
    });

    $("#ProductDetailsForm .product-attr .item input")
        .click(function () {

            $(".conten-attr .item input").parents(".item").removeClass("active");
            $(".conten-attr .item input").removeAttr('checked');

            $(this).parents(".item").toggleClass("active");
            $(this).attr('checked', 'checked');

            var value = this.value;

            $.post("/post/getgallery", { postId: $(this).attr("data-post"), typeId: value },
                function (response) {
                    if (response.success) {
                     
                        $("#gallery").data('royalSlider').destroy();
                        $("#gallery").html(response.data);

                        initGallery();
                    }
                });

            $.post("/post/getpriceforpost", { productId: $(this).attr("data-post"), attributeId: value },
                function (price) {

                    if (price === parseInt(price, 10)) {

                        var priceOriginalUnit = Haravan.formatMoney(price, EGA.options.money_format);

                        //Gía nguyên thủy
                        $("#ProductDetailsForm .product-price-group #span-list-price").html(priceOriginalUnit);
                        $("#detail-two .variant-price ins span ").html(priceOriginalUnit);

                        //product-discount
                        var discount = $('#product-discount').html();
                        var priceDiscount = price * parseInt(discount) / 100;//Số tiền được giảm
                        var priceAfterDicount = price - priceDiscount;//Giá đã trừ tiền giảm
                        var pricePromotionUnit = Haravan.formatMoney(priceAfterDicount, EGA.options.money_format);

                        $('#span-saving-price').html('(' + Haravan.formatMoney(priceDiscount, EGA.options.money_format) + ')');
                        $("#ProductDetailsForm .product-price-group .price").html(pricePromotionUnit);

                        $("#ProductDetailsForm #hddPrice").val(priceAfterDicount);
                    }
                    else {
                        $("#ProductDetailsForm .product-price-group .price").html(price);
                    }

                });

        });

});

function LoadCategoryHome(virtualId, parentId) {
    $.post('/fixitem-content.html',
        { virtualId: virtualId },
        function (response) {
            if (response.success) {
                $(".leftProductContent_" + parentId + "").empty().html(response.data);
            }
        });
}

function showTool() {
    var heightToShow = $(".top-head").height() +
        $(".header").height() +
        $(".nav-menu").height() +
        $("#homeslide").height();

    $(window).scroll(function () {
        if ($(window).scrollTop() >= heightToShow) {
            $(".nav-tools").stop().animate({
                left: '0',
                top: $(window).height() / 3
            });
        } else {
            $(".nav-tools").stop().animate({
                left: '-80px',
                top: heightToShow
            });
        }
    });
}

function goToByScroll(id) {
    // Remove "link" from the ID
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
        scrollTop: $("." + id).offset().top
    },
        'slow');
}

function handleError(msg) {
    $("#msg-error").html(msg).show();
}

function handleSuccess(msg) {
    $("#msg-success").html(msg).show();
}

function formAjax(element) {
    var $form = $(element);
    var options = {
        beforeSend: function () {

            $(".ajax-loading").show();
        },
        dataType: 'json',
        complete: function (responseText, statusText, xhr) {
            var resonse = responseText.responseJSON;
            if (resonse.success) {
                $form[0].reset();
                feature.fancyMsgBox(resonse.title, resonse.message);

            } else {
                feature.fancyMsgBox(resonse.title, resonse.errors);

                $('html, body').animate({
                    scrollTop: 0
                },
                    2000);
            }
            $(".ajax-loading").hide();
        }
    };

    if ($form.valid()) {
        $form.ajaxSubmit(options);
    }
    return false;
}
