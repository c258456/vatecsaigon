﻿using System.Web.Mvc;

namespace App.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute("Admin_default", "Admin/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", area = "Admin", id = UrlParameter.Optional },
                new[] { "App.Admin.Controllers" });

            context.MapRoute("Admin_DefaultPaging", "Admin/{controller}/{action}/page-{page}",
                new { action = "Index", area = "Admin", page = UrlParameter.Optional },
                new[] { "App.Admin.Controllers" });
        }
    }
}