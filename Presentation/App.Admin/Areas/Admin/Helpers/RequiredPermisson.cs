using App.Domain.Entities.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace App.Admin.Helpers
{
    public class RequiredPermisson : AuthorizeAttribute
    {
        private UserManager<IdentityUser, Guid> UserManager => DependencyResolver.Current.GetService<UserManager<IdentityUser, Guid>>();

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.User.Identity.IsAuthenticated)
            {
                return false;
            }
            string name = httpContext.User.Identity.Name;
            if (UserManager.FindByName(name).IsSuperAdmin)
            {
                return true;
            }

            var roles = Roles.Split(',');
            return roles.Any(role => role.Trim().ToLower().Equals("saler") || httpContext.User.IsInRole(role));
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                context.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new
                        {
                            controller = "Security",
                            action = "AccessDined",
                            ReturnUrl = context.HttpContext.Request.Url
                        }));

                return;
            }

            context.Result = new RedirectToRouteResult(
                new RouteValueDictionary(
                    new
                    {
                        controller = "User",
                        action = "Login",
                        ReturnUrl = context.HttpContext.Request.Url
                    }));
        }
    }
}