﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Admin.Helpers;
using App.Core.Caching;
using App.Core.Extensions;
using App.Core.Utilities;
using App.Domain.Stores;
using App.FakeEntity.Stores;
using App.Framework.Utilities;
using App.Service.Account;
using App.Service.Common;
using App.Service.Media;
using App.Service.Settings;
using App.Service.Stores;
using AutoMapper;
using Newtonsoft.Json;

namespace App.Admin.Controllers
{
    public class StoreController : BaseAdminUploadController
    {
        private readonly IStoreService _storeService;
        private readonly IWorkContext _webContext;
        private readonly IImageService _imageService;
        private readonly ISettingService _settingService;

        public StoreController(ICacheManager cacheManager, IStoreService storeService,
            IUserStoreService userStoreService, IWorkContext webContext, IImageService imageService, ISettingService settingService) : base(
            cacheManager)
        {
            _storeService = storeService;
            _webContext = webContext;
            _imageService = imageService;
            _settingService = settingService;
        }

        [RequiredPermisson(Roles = "ViewStore, Saler")]
        public ActionResult Index()
        {
            if (!_webContext.CurrentSaler.Stores.IsAny())
            {
                return View();
            }

            int storeId = _webContext.CurrentSaler.Stores.FirstOrDefault().Id;
            var existingStore = _storeService.GetById(storeId);

            if (existingStore == null)
            {
                return View();
            }

            var storeViewModel = Mapper.Map<Store, StoreViewModel>(existingStore);

            return View(storeViewModel);
        }

        [HttpPost]
        public ActionResult UpdateInfo(StoreViewModel model)
        {
            if (!Request.IsAjaxRequest())
            {
                var msg = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                    .Select(v => v.ErrorMessage + " " + v.Exception));

                ModelState.AddModelError("", msg);

                return Json(new
                {
                    title = "Cập nhật thông tin người bán",
                    success = false,
                    errors = msg
                });
            }

            try
            {
                var existingStore = _storeService.GetById(model.Id);
                existingStore.Slogan = model.Slogan;
                existingStore.ShortDesc = model.ShortDesc;
                existingStore.Url = existingStore.Name;

                _storeService.Update(existingStore);

                return Json(new
                {
                    title = "Cập nhật thông tin người bán",
                    success = true,
                    message = "Cập nhật thông tin người bán thành công."
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    title = "Cập nhật thông tin người bán",
                    success = false,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult UpdateAvatarStore(int storeId)
        {
            if (!Request.IsAjaxRequest())
            {
                var msg = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                    .Select(v => v.ErrorMessage + " " + v.Exception));

                ModelState.AddModelError("", msg);

                return Json(new
                {
                    title = "Cập nhật avartar",
                    success = false,
                    errors = msg
                });
            }

            try
            {
                if (!System.Web.HttpContext.Current.Request.Files.AllKeys.IsAny())
                {
                    return Json(new
                    {
                        title = "Cập nhật avartar",
                        success = false,
                        message = "Request is null"
                    });
                }

                var file = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                HttpPostedFileBase filebase = new HttpPostedFileWrapper(file);

                var existingStore = _storeService.GetById(storeId);
                existingStore.ImageUrl = RenderImageSize(existingStore.Name, filebase);

                _storeService.Update(existingStore);

                return Json(new
                {
                    title = "Cập nhật avartar",
                    success = true,
                    message = "Đăng ký shop thành công, chúng tôi sẽ xét duyệt yêu cầu và liên lạc với bạn ngay khi có thể."
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    title = "Cập nhật avartar",
                    success = false,
                    message = ex.Message
                });
            }
        }

        private string RenderImageSize(string storeName, HttpPostedFileBase filebase)
        {
            var isPng = _settingService.GetSetting("Menu.pngFormat", 0);
            var folderName = CommonHelper.FolderName(storeName);

            var fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
            var fileExtension = Path.GetExtension(filebase.FileName);
            var fileNameFormat = fileName.FileNameFormat(fileExtension);

            var sizeWidthMd = _settingService.GetSetting("Menu.WidthMediumSize", ImageSize.WidthDefaultSize);
            var sizeHeighthMd = _settingService.GetSetting("Menu.HeightMediumSize", ImageSize.HeightDefaultSize);

            _imageService.CropAndResizeImage(filebase, $"{Constant.StoreFolder}{folderName}/", fileNameFormat, sizeWidthMd, sizeHeighthMd, isPng != 0);
            return $"{Constant.StoreFolder}{folderName}/{fileNameFormat}";
        }

    }
}