﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using App.Service.Posts;
using System.Web.Mvc;
using App.Admin.Helpers;
using App.Core.Caching;
using App.Core.Extensions;
using App.Core.Infrastructure;
using App.Core.Logging;
using App.Core.Utilities;
using App.Framework.Utilities;
using App.Service.Attributes;
using App.Service.Common;
using App.Service.Galleries;
using App.Service.Languages;
using App.Service.Manufacturers;
using App.Service.Media;
using App.Service.Menus;
using App.Service.Orders;
using App.Service.Settings;
using App.Domain.Common;
using App.Domain.Entities.Attribute;
using App.Domain.Galleries;
using App.Domain.Languages;
using App.Domain.Orders;
using App.Domain.Posts;
using App.FakeEntity.Galleries;
using App.FakeEntity.Posts;
using App.Service.GenericControls;
using AutoMapper;
using Resources;

namespace App.Admin.Controllers
{
    public class PostController : BaseAdminUploadController
    {
        private readonly IAttributeValueService _attributeValueService;

        private readonly IMenuLinkService _menuLinkService;

        private readonly IPostService _postService;

        private readonly IAttributeService _attributeService;

        private readonly IGalleryService _galleryService;

        private readonly IImageService _imageService;

        private readonly ILanguageService _languageService;

        private readonly ILocalizedPropertyService _localizedPropertyService;

        private readonly IPostGalleryService _postGalleryService;

        private readonly IManufacturerService _manufacturerService;
        private readonly IOrderItemService _orderItemService;
        private readonly ISettingService _settingService;

        private readonly IWorkContext _webContext;

        public PostController(
            IPostService postService
            , IMenuLinkService menuLinkService
            , IAttributeValueService attributeValueService
            , IGalleryService galleryService
            , IImageService imageService
            , IAttributeService attributeService
            , ILanguageService languageService
            , ILocalizedPropertyService localizedPropertyService
            , IPostGalleryService postGalleryService
            , IGenericControlService genericControlService
            , ICacheManager cacheManager
            , IManufacturerService manufacturerService
            , IOrderItemService orderItemService
            , ISettingService settingService
            , IWorkContext webContext) : base(cacheManager)
        {
            _postService = postService;
            _menuLinkService = menuLinkService;
            _attributeValueService = attributeValueService;
            _galleryService = galleryService;
            _imageService = imageService;
            _attributeService = attributeService;
            _languageService = languageService;
            _localizedPropertyService = localizedPropertyService;
            _postGalleryService = postGalleryService;
            _manufacturerService = manufacturerService;
            _orderItemService = orderItemService;
            _settingService = settingService;
            _webContext = webContext;
        }

        [RequiredPermisson(Roles = "CreatePost,Saler")]
        public ActionResult Create()
        {
            var model = new PostViewModel
            {
                OrderDisplay = 1,
                Status = (int)Status.Enable
            };

            //Add locales to model
            AddLocales(_languageService, model.Locales);

            return View(model);
        }

        [HttpPost]
        [RequiredPermisson(Roles = "CreatePost,Saler")]
        [ValidateInput(false)]
        public ActionResult Create(PostViewModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var messages = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                                                          .Select(v => v.ErrorMessage + " " + v.Exception));
                    ModelState.AddModelError("", messages);

                    return View(model);
                }

                //Check existing product code
                var validProductCode = _postService.FindBy(x => x.ProductCode.Equals(model.ProductCode) && x.Id != model.Id, true);
                if (validProductCode.IsAny())
                {
                    ModelState.AddModelError("", "Mã sản phẩm đã tồn tại.");

                    return View(model);
                }

                var titleNonAccent = model.Title.NonAccent();
                var existingPost = _postService.GetListSeoUrl(titleNonAccent);

                model.SeoUrl = titleNonAccent;
                if (existingPost.Any(x => x.Id != model.Id))
                {
                    model.SeoUrl = string.Concat(model.SeoUrl, "-", existingPost.Count());
                }

                PostPictures(model);

                var menuId = model.MenuId;
                var i = 0;
                if (menuId.GetValueOrDefault() > i && menuId.HasValue)
                {
                    var exstingMenu = _menuLinkService.GetMenu(menuId.Value);
                    model.VirtualCatUrl = exstingMenu.VirtualSeoUrl;
                    model.VirtualCategoryId = exstingMenu.VirtualId;
                }

                //Update post's store
                model.StoreId = _webContext.CurrentSaler.Stores.IsAny()
                     ? _webContext.CurrentSaler.Stores.FirstOrDefault().Id
                     : 0;

                var postMapping = Mapper.Map<PostViewModel, Post>(model);

                //Gallery image
                var galleryImages = PostImageAttributeHandler(model);
                if (galleryImages.IsAny())
                {
                    postMapping.GalleryImages = galleryImages;
                }

                //AttributeValue
                var attrsValue = new List<AttributeValue>();
                var reqValues = Request["Values"];
                if (!string.IsNullOrEmpty(reqValues))
                {
                    foreach (var reqValue in reqValues.Split(',').ToList())
                    {
                        attrsValue.Add(_attributeValueService.GetById(int.Parse(reqValue)));
                    }
                }
                if (attrsValue.IsAny())
                {
                    postMapping.AttributeValues = attrsValue;
                }

                _postService.Create(postMapping);

                //Update Localized   
                foreach (var localized in model.Locales)
                {
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.Title, localized.Title, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.ProductCode, localized.ProductCode, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.ShortDesc, localized.ShortDesc, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.Description, localized.Description, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.TechInfo, localized.TechInfo, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.SeoUrl, localized.SeoUrl, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.MetaTitle, localized.MetaTitle, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.MetaKeywords, localized.MetaKeywords, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.MetaDescription, localized.MetaDescription, localized.LanguageId);
                }

                Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.CreateSuccess, FormUI.Post)));
                return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                       returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                    ? (ActionResult)RedirectToAction("Index")
                    : Redirect(returnUrl);
            }
            catch (Exception ex)
            {
                LogText.Log(string.Concat("Post.Create: ", ex.Message));
                ModelState.AddModelError("", ex.Message);

                return View(model);
            }
        }

        [RequiredPermisson(Roles = "DeletePost,Saler")]
        public ActionResult Delete(string[] ids)
        {
            try
            {
                if (ids.IsAny())
                {
                    var posts = new List<Post>();
                    var galleryImages = new List<GalleryImage>();
                    var orderItems = new List<OrderItem>();
                    IEnumerable<LocalizedProperty> localizedProperties = null;

                    foreach (var item in ids)
                    {
                        var id = int.Parse(item);
                        var post = _postService.Get(x => x.Id == id);

                        if (post.GalleryImages.IsAny())
                        {
                            galleryImages.AddRange(post.GalleryImages.ToList());
                        }

                        post.AttributeValues.ToList().ForEach(att => post.AttributeValues.Remove(att));
                        posts.Add(post);

                        var orderItem = _orderItemService.GetByPostId(id);
                        if (orderItem != null)
                        {
                            orderItems.Add(orderItem);
                        }

                        localizedProperties = _localizedPropertyService.GetByEntityId(id);
                    }

                    _galleryService.BatchDelete(galleryImages);
                    _orderItemService.BatchDelete(orderItems);
                    _localizedPropertyService.BatchDelete(localizedProperties);
                    _postService.BatchDelete(posts);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                LogText.Log(string.Concat("Post.Delete: ", ex.Message));
            }

            return RedirectToAction("Index");
        }

        [RequiredPermisson(Roles = "CreatePost,Saler")]
        public ActionResult DeleteGallery(int postId, int galleryId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false });
            }

            try
            {
                var galleryImage = _galleryService.Get(x => x.PostId == postId && x.Id == galleryId);
                _galleryService.Delete(galleryImage);

                var imgPathBg = Server.MapPath(string.Concat("~/", galleryImage.ImageBig));
                var imgPathThm = Server.MapPath(string.Concat("~/", galleryImage.ImageThumbnail));

                if (System.IO.File.Exists(imgPathBg))
                {
                    System.IO.File.Delete(imgPathBg);
                }

                if (System.IO.File.Exists(imgPathThm))
                {
                    System.IO.File.Delete(imgPathThm);
                }

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, messages = ex.Message });
            }
        }

        [RequiredPermisson(Roles = "EditPost,Saler")]
        public ActionResult Edit(int id)
        {
            var existingPost = _postService.GetById(id);

            var modelMap = Mapper.Map<Post, PostViewModel>(existingPost);

            ViewBag.Galleries = existingPost.GalleryImages;

            //Add Locales to model
            AddLocales(_languageService, modelMap.Locales, (locale, languageId) =>
            {
                locale.Id = modelMap.Id;
                locale.LocalesId = modelMap.Id;
                locale.Title = modelMap.GetLocalized(x => x.Title, id, languageId, false, false);
                locale.ProductCode = modelMap.GetLocalized(x => x.ProductCode, id, languageId, false, false);
                locale.ShortDesc = modelMap.GetLocalized(x => x.ShortDesc, id, languageId, false, false);
                locale.Description = modelMap.GetLocalized(x => x.Description, id, languageId, false, false);
                locale.TechInfo = modelMap.GetLocalized(x => x.TechInfo, id, languageId, false, false);
                locale.MetaTitle = modelMap.GetLocalized(x => x.MetaTitle, id, languageId, false, false);
                locale.MetaKeywords = modelMap.GetLocalized(x => x.MetaKeywords, id, languageId, false, false);
                locale.MetaDescription = modelMap.GetLocalized(x => x.MetaDescription, id, languageId, false, false);
                locale.SeoUrl = modelMap.GetLocalized(x => x.SeoUrl, id, languageId, false, false);
            });

            return View(modelMap);
        }

        [HttpPost]
        [RequiredPermisson(Roles = "EditPost,Saler")]
        [ValidateInput(false)]
        public ActionResult Edit(PostViewModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var messages = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                                                          .Select(v => v.ErrorMessage + " " + v.Exception));
                    ModelState.AddModelError("", messages);

                    return View(model);
                }

                var existingPost = _postService.GetById(model.Id, false);

                var titleNonAccent = model.Title.NonAccent();
                var bySeoUrl = _menuLinkService.GetListSeoUrl(titleNonAccent, false);

                model.SeoUrl = model.Title.NonAccent();
                if (bySeoUrl.Any(x => x.Id != model.Id))
                {
                    var postViewModel = model;
                    postViewModel.SeoUrl = string.Concat(postViewModel.SeoUrl, "-", bySeoUrl.Count());
                }

                PostPictures(model);

                var menuId = model.MenuId;
                if (menuId.GetValueOrDefault() > 0 && menuId.HasValue)
                {
                    menuId = model.MenuId;
                    var menuLink = _menuLinkService.GetMenu(menuId.Value, false);
                    model.VirtualCatUrl = menuLink.VirtualSeoUrl;
                    model.VirtualCategoryId = menuLink.VirtualId;
                }

                //GalleryImage
                var galleryImages = PostImageAttributeHandler(model);
                if (galleryImages.IsAny())
                {
                    existingPost.GalleryImages = galleryImages;
                }

                //AttributeValue
                var attrValues = new List<AttributeValue>();
                var attrIds = new List<int>();
                var reqValues = Request["Values"];
                if (!string.IsNullOrEmpty(reqValues))
                {
                    foreach (var reqValue in reqValues.Split(',').ToList())
                    {
                        var num1 = int.Parse(reqValue);
                        attrIds.Add(num1);
                        attrValues.Add(_attributeValueService.GetById(num1, false));
                    }

                    if (attrIds.IsAny())
                    {
                        (
                            from x in existingPost.AttributeValues
                            where !attrIds.Contains(x.Id)
                            select x).ToList().ForEach(att => existingPost.AttributeValues.Remove(att));
                    }
                }

                existingPost.AttributeValues = attrValues;

                //Update post's store
                model.StoreId = _webContext.CurrentSaler.Stores.IsAny()
                    ? _webContext.CurrentSaler.Stores.FirstOrDefault().Id
                    : 0;

                var postMapping = Mapper.Map(model, existingPost);

                _postService.Update(existingPost);

                //Update GalleryImage
                if (attrValues.IsAny())
                {
                    foreach (var attrValue in attrValues)
                    {
                        var galleryImage = _galleryService.Get(x =>
                            x.AttributeValueId == attrValue.Id && x.PostId == model.Id);

                        if (galleryImage == null)
                        {
                            continue;
                        }

                        var priceValue = decimal.Parse(Request[attrValue.Id.ToString()]);
                        galleryImage.Price = priceValue;

                        _galleryService.Update(galleryImage);
                    }
                }

                //Update Localized
                foreach (var localized in model.Locales)
                {
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.Title, localized.Title,
                        localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.ProductCode,
                        localized.ProductCode, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.ShortDesc, localized.ShortDesc,
                        localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.Description,
                        localized.Description, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.TechInfo, localized.TechInfo,
                        localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.SeoUrl, localized.SeoUrl,
                        localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.MetaTitle, localized.MetaTitle,
                        localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.MetaKeywords,
                        localized.MetaKeywords, localized.LanguageId);
                    _localizedPropertyService.SaveLocalizedValue(postMapping, x => x.MetaDescription,
                        localized.MetaDescription, localized.LanguageId);
                }

                Response.Cookies.Add(new HttpCookie("system_message",
                    string.Format(MessageUI.UpdateSuccess, FormUI.Post)));

                return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                       returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                    ? (ActionResult)RedirectToAction("Index")
                    : Redirect(returnUrl);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                LogText.Log(string.Concat("Post.Edit: ", ex.Message));

                return View(model);
            }
        }

        [RequiredPermisson(Roles = "ViewPost,Saler")]
        public ActionResult Index(int page = 1, string keywords = "")
        {
            ViewBag.Keywords = keywords;
            var sortingPagingBuilder = new SortingPagingBuilder
            {
                Keywords = keywords,
                Sorts = new SortBuilder
                {
                    ColumnName = "CreatedDate",
                    ColumnOrder = SortBuilder.SortOrder.Descending
                }
            };
            var paging = new Paging
            {
                PageNumber = page,
                PageSize = CommonHelper.PageSize,
                TotalRecord = 0
            };

            int storeId = _webContext.CurrentSaler.Stores.IsAny() ? _webContext.CurrentSaler.Stores.FirstOrDefault().Id : 0;

            var posts = _postService.PagedList(sortingPagingBuilder, paging, storeId);
            if (!posts.IsAny())
            {
                return View(posts);
            }

            var pageInfo = new Helper.PageInfo(CommonHelper.PageSize, page, paging.TotalRecord,
                i => Url.Action("Index", new { page = i, keywords }));

            ViewBag.PageInfo = pageInfo;

            return View(posts);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.RouteData.Values["action"].Equals("edit") || filterContext.RouteData.Values["action"].Equals("create"))
            {
                var manufacturers = _manufacturerService.FindBy(x => x.Status == (int)Status.Enable);
                if (manufacturers.IsAny())
                {
                    ViewBag.Manufacturers = manufacturers;
                }

                var attributes = _attributeService.FindBy(x => x.Status == (int)Status.Enable);
                if (attributes.IsAny())
                {
                    ViewBag.Attributes = attributes;
                }
            }
        }

        #region Post image gallery

        [HttpPost]
        public ActionResult PostGalleryAdd(int postId)
        {
            var byId = _postService.GetById(postId, false);
            var titleOriginal = byId.Title;

            var files = Request.Files;
            if (files.Count <= 0)
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            var count = files.Count - 1;
            var num = 0;
            var allKeys = files.AllKeys;
            foreach (var key in allKeys)
            {
                if (num <= count)
                {
                    if (!key.Equals("Image"))
                    {
                        var file = files[num];
                        if (file.ContentLength > 0)
                        {
                            var postGallery = new PostGallery
                            {
                                OrderDisplay = _postGalleryService.GetMaxOrderDiplay(postId),
                                //IsAvatar = i == 0 ? (int)Status.Enable : (int)Status.Disable,
                                PostId = postId,
                                Status = (int)Status.Enable
                            };

                            var folderName = CommonHelper.FolderName(titleOriginal);
                            var fileExtension = Path.GetExtension(file.FileName);

                            var fileNameBg = $"slide.{ titleOriginal}".FileNameFormat(fileExtension);
                            var fileNameMd = $"slide.{ titleOriginal}".FileNameFormat(fileExtension);
                            var fileNameSm = $"slide.{ titleOriginal}".FileNameFormat(fileExtension);

                            var sizeWidthBg = _settingService.GetSetting("Post.GalleryWidthBigSize", ImageSize.WidthDefaultSize);
                            var sizeHeightBg = _settingService.GetSetting("Post.GalleryHeightBigSize", ImageSize.HeightDefaultSize);
                            var sizeWidthMd = _settingService.GetSetting("Post.GalleryWidthMediumSize", ImageSize.WidthDefaultSize);
                            var sizeHeightMd = _settingService.GetSetting("Post.GalleryHeightMediumSize", ImageSize.HeightDefaultSize);
                            var sizeWidthSm = _settingService.GetSetting("Post.GalleryWidthSmallSize", ImageSize.WidthDefaultSize);
                            var sizeHeightSm = _settingService.GetSetting("Post.GalleryHeightSmallSize", ImageSize.HeightDefaultSize);

                            _imageService.CropAndResizeImage(file, $"{Constant.PostFolder}{folderName}/", fileNameBg, sizeWidthBg, sizeHeightBg);
                            _imageService.CropAndResizeImage(file, $"{Constant.PostFolder}{folderName}/", fileNameMd, sizeWidthMd, sizeHeightMd);
                            _imageService.CropAndResizeImage(file, $"{Constant.PostFolder}{folderName}/", fileNameSm, sizeWidthSm, sizeHeightSm);

                            postGallery.ImageBigSize = $"{Constant.PostFolder}{folderName}/{fileNameBg}";
                            postGallery.ImageMediumSize = $"{Constant.PostFolder}{folderName}/{fileNameMd}";
                            postGallery.ImageSmallSize = $"{Constant.PostFolder}{folderName}/{fileNameSm}";

                            _postGalleryService.Create(postGallery);
                        }
                        num++;
                    }
                    else
                    {
                        num++;
                    }
                }
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PostGalleryEdit(PostGalleryViewModel model)
        {
            var postGallery = _postGalleryService.GetById(model.Id);

            if (postGallery == null)
            {
                return Json(
                    new
                    {
                        succes = true
                    }
                    , JsonRequestBehavior.AllowGet);
            }

            model.ImageBigSize = postGallery.ImageBigSize;
            model.ImageMediumSize = postGallery.ImageMediumSize;
            model.ImageSmallSize = postGallery.ImageSmallSize;
            model.OrderDisplay = postGallery.OrderDisplay;
            model.IsAvatar = postGallery.IsAvatar;
            model.Status = postGallery.Status;

            var postGalleryMap = Mapper.Map(model, postGallery);
            _postGalleryService.Update(postGalleryMap);

            return Json(
                new
                {
                    succes = true
                }
                , JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PostGalleryList(int postId)
        {
            var postGalleryList = _postGalleryService.GetByPostId(postId);

            var posts = await Task.FromResult(
                from x in postGalleryList
                orderby x.OrderDisplay
                select x);

            var jsonResult = Json(new { data = posts }, JsonRequestBehavior.AllowGet);

            return jsonResult;
        }

        public ActionResult DeletePostGallery(int postId, int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false });
            }

            try
            {
                var postGallery = _postGalleryService.Get(x => x.PostId == postId && x.Id == id);
                _postGalleryService.Delete(postGallery);

                var imgPathBg = Server.MapPath(string.Concat("~/", postGallery.ImageBigSize));
                var imgPathMd = Server.MapPath(string.Concat("~/", postGallery.ImageMediumSize));
                var imgPathSm = Server.MapPath(string.Concat("~/", postGallery.ImageSmallSize));

                if (System.IO.File.Exists(imgPathBg))
                {
                    System.IO.File.Delete(imgPathBg);
                }
                if (System.IO.File.Exists(imgPathMd))
                {
                    System.IO.File.Delete(imgPathMd);
                }
                if (System.IO.File.Exists(imgPathSm))
                {
                    System.IO.File.Delete(imgPathSm);
                }

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, messages = ex.Message });
            }
        }

        public ActionResult PostGalleryChangeStatus(int postId, int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false });
            }

            try
            {
                var postGallery = _postGalleryService.Get(x => x.PostId == postId && x.Id == id);
                //var oldStatus = postGallery.Status;
                postGallery.Status = postGallery.Status == (int)Status.Enable ? (int)Status.Disable : (int)Status.Enable;

                _postGalleryService.Update(postGallery);

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, messages = ex.Message });
            }
        }

        public ActionResult SetAvatarImage(int postId, int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "IsAjaxRequest is false" });
            }

            try
            {
                var postGallerys = _postGalleryService.GetByPostId(postId, false);
                if (!postGallerys.IsAny())
                {
                    return Json(new { success = false, messages = "PostGallery has not existing" });
                }

                //First, update old isAvatar is disable
                foreach (var postGallery in postGallerys)
                {
                    postGallery.IsAvatar = (int)Status.Disable;
                    _postGalleryService.Update(postGallery);
                }

                //Second, update new isAvatar is enable
                var existingGalleries = postGallerys.FirstOrDefault(x => x.Id == id && x.Status == (int)Status.Enable);
                existingGalleries.IsAvatar = (int)Status.Enable;
                _postGalleryService.Update(existingGalleries);

                var existingPost = _postService.GetById(postId, false);
                existingPost.ImageBigSize = existingGalleries.ImageBigSize;
                existingPost.ImageMediumSize = existingGalleries.ImageMediumSize;
                existingPost.ImageSmallSize = existingGalleries.ImageSmallSize;

                _postService.Update(existingPost);

                return Json(new { success = true, messages = existingGalleries.ImageSmallSize });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, messages = ex.Message });
            }
        }

        public ActionResult PostGalleryChangeOrder(int id, int newPosition)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false });
            }

            try
            {
                var postGallery = _postGalleryService.Get(x => x.Id == id);
                postGallery.OrderDisplay = newPosition;

                _postGalleryService.Update(postGallery);

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, messages = ex.Message });
            }
        }

        #endregion

        private void PostPictures(PostViewModel model)
        {
            if (model.Image == null || model.Image.ContentLength <= 0)
            {
                return;
            }

            var folderName = CommonHelper.FolderName(model.Title);

            var fileNameOriginal = Path.GetFileNameWithoutExtension(model.Image.FileName);
            var fileExtension = Path.GetExtension(model.Image.FileName);

            var fileNameBg = fileNameOriginal.FileNameFormat(fileExtension);
            var fileNameMd = fileNameOriginal.FileNameFormat(fileExtension);
            var fileNameSm = fileNameOriginal.FileNameFormat(fileExtension);

            var sizeWidthBg = _settingService.GetSetting("Post.WidthBigSize", ImageSize.WidthDefaultSize);
            var sizeHeightBg = _settingService.GetSetting("Post.HeightBigSize", ImageSize.HeightDefaultSize);
            var sizeWidthMd = _settingService.GetSetting("Post.WidthMediumSize", ImageSize.WidthDefaultSize);
            var sizeHeightMd = _settingService.GetSetting("Post.HeightMediumSize", ImageSize.HeightDefaultSize);
            var sizeWidthSm = _settingService.GetSetting("Post.WidthSmallSize", ImageSize.WidthDefaultSize);
            var sizeHeightSm = _settingService.GetSetting("Post.HeightSmallSize", ImageSize.HeightDefaultSize);

            _imageService.CropAndResizeImage(model.Image, $"{Constant.PostFolder}{folderName}/", fileNameBg, sizeWidthBg, sizeHeightBg);
            _imageService.CropAndResizeImage(model.Image, $"{Constant.PostFolder}{folderName}/", fileNameMd, sizeWidthMd, sizeHeightMd);
            _imageService.CropAndResizeImage(model.Image, $"{Constant.PostFolder}{folderName}/", fileNameSm, sizeWidthSm, sizeHeightSm);

            model.ImageBigSize = $"{Constant.PostFolder}{folderName}/{fileNameBg}";
            model.ImageMediumSize = $"{Constant.PostFolder}{folderName}/{fileNameMd}";
            model.ImageSmallSize = $"{Constant.PostFolder}{folderName}/{fileNameSm}";
        }

        private List<GalleryImage> PostImageAttributeHandler(PostViewModel model)
        {
            if (model == null)
            {
                return null;
            }

            var files = Request.Files;
            if (files.Count <= 0)
            {
                return new List<GalleryImage>(); ;
            }

            var galleryImages = new List<GalleryImage>();
            var folderName = CommonHelper.FolderName(model.Title);

            var count = files.Count - 1;
            var num = 0;

            var allKeys = files.AllKeys;
            int i;
            for (i = 0; i < allKeys.Length; i++)
            {
                var keys = allKeys[i];

                if (num <= count)
                {
                    if (!keys.Equals("Image"))
                    {
                        var keysNotCharSpecial = keys.Replace("[]", "");
                        var file = files[num];
                        if (file != null && file.ContentLength > 0)
                        {
                            var key = Request[keysNotCharSpecial];
                            var galleryImageViewModel = new GalleryImageViewModel
                            {
                                PostId = model.Id,
                                AttributeValueId = int.Parse(keysNotCharSpecial)
                            };

                            var fileNameOrginal = Path.GetFileNameWithoutExtension(file.FileName);
                            var fileExtension = Path.GetExtension(file.FileName);

                            var fileNameBg = $"attr.{ fileNameOrginal}".FileNameFormat(fileExtension);
                            var fileNameThum = $"attr.{ fileNameOrginal}".FileNameFormat(fileExtension);

                            var sizeWidthBg = _settingService.GetSetting("Post.AttributeWithBigSize", ImageSize.WidthDefaultSize);
                            var sizeHeighthBg = _settingService.GetSetting("Post.AttributeHeightBigSize", ImageSize.HeightDefaultSize);
                            var sizeWidthThum = _settingService.GetSetting("Post.AttributeWidthThumSize", ImageSize.WidthDefaultSize);
                            var sizeHeightThum = _settingService.GetSetting("Post.AttributeHeightThumSize", ImageSize.HeightDefaultSize);

                            _imageService.CropAndResizeImage(file, $"{Constant.PostFolder}{folderName}/", fileNameBg, sizeWidthBg, sizeHeighthBg);
                            _imageService.CropAndResizeImage(file, $"{Constant.PostFolder}{folderName}/", fileNameThum, sizeWidthThum, sizeHeightThum);

                            galleryImageViewModel.ImageBig = $"{Constant.PostFolder}{folderName}/{fileNameBg}";
                            galleryImageViewModel.ImageThumbnail = $"{Constant.PostFolder}{folderName}/{fileNameThum}";

                            galleryImageViewModel.OrderDisplay = num;
                            galleryImageViewModel.Status = 1;
                            galleryImageViewModel.Title = model.Title;
                            galleryImageViewModel.Price = double.Parse(key);

                            galleryImages.Add(Mapper.Map<GalleryImage>(galleryImageViewModel));
                        }
                        num++;
                    }
                    else
                    {
                        num++;
                    }
                }
            }

            return galleryImages;
        }
    }
}