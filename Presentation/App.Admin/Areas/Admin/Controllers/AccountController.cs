using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using App.Admin.Helpers;
using App.Core.Extensions;
using App.Core.Logging;
using App.Core.Utilities;
using App.Domain.Entities.Identity;
using App.FakeEntity.User;
using App.Framework.Utilities;
using App.Service.Account;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Resources;

namespace App.Admin.Controllers
{
    public class AccountController : BaseIdentityController
    {
        private readonly IUserService _userService;

        private readonly RoleManager<IdentityRole, Guid> _roleManager;

        public AccountController(IUserService userService, UserManager<IdentityUser, Guid> userManager
            , RoleManager<IdentityRole, Guid> roleManager
            ) : base(userManager)
        {
            _userService = userService;
            _roleManager = roleManager;
        }

        [HttpGet]
        [RequiredPermisson(Roles = "CreateEditAccount")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [RequiredPermisson(Roles = "CreateEditAccount")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterFormViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var identityUser = new IdentityUser
            {
                UserName = model.UserName,
                Address = model.Address,
                FirstName = model.FirstName,
                LastName = model.LastName,
                MiddleName = model.MiddleName,
                Phone = model.Phone,
                Email = model.Email,
                City = model.City,
                State = model.State,
                IsLockedOut = false,
                IsSuperAdmin = false,
                Created = DateTime.UtcNow
            };

            var identityResult = await UserManager.CreateAsync(identityUser, model.Password);

            if (!identityResult.Succeeded)
            {
                AddErrors(identityResult);

                return View(model);
            }

            if (!model.IsSuperAdmin)
            {
                var role = Request["roles"];
                if (!string.IsNullOrEmpty(role))
                {
                    var roles = role.Split(',');
                    await UserManager.AddToRolesAsync(identityUser.Id, roles);
                }
            }

            Response.Cookies.Add(new HttpCookie("system_message",
                string.Format(MessageUI.CreateSuccess, FormUI.Account)));

            return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                   returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                ? (ActionResult) RedirectToAction("Index")
                : Redirect(returnUrl);
        }

        [HttpGet]
        [RequiredPermisson(Roles = "CreateEditAccount")]
        public async Task<ActionResult> Edit(string id)
        {
            var userId = Guid.Parse(id);
            var existingUser = await UserManager.FindByIdAsync(userId);

            var registerForm = Mapper.Map<EditModel>(existingUser);

            return View(registerForm);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var messages = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage + " " + v.Exception));

                    ModelState.AddModelError("", messages);

                    return View(model);
                }

                model.Created = DateTime.Now;
                var existingUser = UserManager.FindById(model.Id);
                existingUser = Mapper.Map(model, existingUser);

                var identityResult = await UserManager.UpdateAsync(existingUser);
                if (identityResult.Succeeded)
                {
                    if (model.IsSuperAdmin)
                    {
                        var roles = UserManager.GetRoles(model.Id);
                        UserManager.RemoveFromRoles(model.Id, roles.ToArray());
                    }
                    else
                    {
                        var role = Request["roles"];
                        if (!string.IsNullOrEmpty(role))
                        {
                            var userRoles = UserManager.GetRoles(model.Id);
                            UserManager.RemoveFromRoles(model.Id, userRoles.ToArray());

                            var roles = role.Split(',');
                            UserManager.AddToRoles(model.Id, roles);
                        }
                    }

                    Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.UpdateSuccess, FormUI.Account)));
                    return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                           returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                        ? (ActionResult) RedirectToAction("Index")
                        : Redirect(returnUrl);
                }

                AddErrors(identityResult);

                return View(model);
            }
            catch (Exception ex)
            {
                LogText.Log(string.Concat("Account.Update: ", ex.Message));
                ModelState.AddModelError("", ex.Message);

                return View(model);
            }
        }

        [RequiredPermisson(Roles = "DeleteAccount")]
        public ActionResult Delete(string[] ids)
        {
            try
            {
                foreach (var id in ids)
                {
                    var userId = Guid.Parse(id);

                    var userLogins = UserManager.GetLogins(userId);
                    foreach (var userLogin in userLogins)
                    {
                        UserManager.RemoveLogin(userId, userLogin);
                    }

                    var userRoles = UserManager.GetRoles(userId);

                    UserManager.RemoveFromRoles(userId, userRoles.ToArray());

                    var user = UserManager.FindByIdAsync(userId).Result;
                    UserManager.Update(user);
                    UserManager.DeleteAsync(user);
                }
            }
            catch (Exception ex)
            {
                LogText.Log(string.Concat("Post.Delete: ", ex.Message));
                ModelState.AddModelError("", ex.Message);
            }

            return View("Index");
        }

        [RequiredPermisson(Roles = "ViewAccount")]
        public async Task<ActionResult> Index(int page = 1, string keywords = "")
        {
            ViewBag.Keywords = keywords;
            var sortingPagingBuilder = new SortingPagingBuilder
            {
                Keywords = keywords
            };
            var sortBuilder = new SortBuilder
            {
                ColumnName = "UserName",
                ColumnOrder = SortBuilder.SortOrder.Descending
            };
            sortingPagingBuilder.Sorts = sortBuilder;
            var sortingPagingBuilder1 = sortingPagingBuilder;
            var paging = new Paging
            {
                PageNumber = page,
                PageSize = PageSize,
                TotalRecord = 0
            };
          

            var users = await _userService.PagedList(sortingPagingBuilder1, paging);
            if (users.IsAny())
            {
                var pageInfo = new Helper.PageInfo(CommonHelper.PageSize, page, paging.TotalRecord, i => Url.Action("Index", new { page = i, keywords }));
                ViewBag.PageInfo = pageInfo;
            }

            return View(users);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.RouteData.Values["action"].Equals("create"))
            {
                ViewBag.Roles = _roleManager.Roles.ToList();
            }

            else if (filterContext.RouteData.Values["action"].Equals("edit"))
            {
                var roles = _roleManager.Roles.ToList();

                var userId = filterContext.RouteData.Values["id"].ToString(); 
                var rolesByUser = UserManager.GetRoles(Guid.Parse(userId));

                foreach (var role in roles)
                {
                    foreach (var roleUser in rolesByUser)
                    {
                        if (role.Name == roleUser)
                        {
                            role.Selected = true;
                            break;
                        }
                    }
                }

                ViewBag.Roles = roles;
            }
        }
    }
}