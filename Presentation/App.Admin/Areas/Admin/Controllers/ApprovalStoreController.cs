using App.Core.Extensions;
using App.Core.Logging;
using App.Core.Utilities;
using App.Domain.Stores;
using App.FakeEntity.Stores;
using App.Framework.Utilities;
using App.Service.Account;
using App.Service.Stores;
using AutoMapper;
using Resources;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using App.Admin.Helpers;
using App.Core.Caching;

namespace App.Admin.Controllers
{
    public class ApprovalStoreController : BaseAdminUploadController
    {
        private readonly IStoreService _storeService;
        private readonly IUserStoreService _userStoreService;

        public ApprovalStoreController(IStoreService storeService, IUserStoreService userStoreService, ICacheManager cacheManager):base(cacheManager)

        {
            _storeService = storeService;
            _userStoreService = userStoreService;
        }

        public ActionResult Edit(int id)
        {
            var existingStore = _storeService.GetById(id);
            var storeViewModel = Mapper.Map<Store, StoreViewModel>(existingStore);

            return View(storeViewModel);
        }

        [HttpPost]
        [RequiredPermisson(Roles = "ViewStore")]
        public ActionResult Edit(StoreViewModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var messages = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage + " " + v.Exception));

                    ModelState.AddModelError("", messages);
                    return View(model);
                }

                var store = Mapper.Map<StoreViewModel, Store>(model);
                _storeService.Update(store);

                Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.UpdateSuccess, FormUI.Store)));
                return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                       returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                    ? (ActionResult)RedirectToAction("Index")
                    : Redirect(returnUrl);
            }
            catch (Exception ex)
            {
                LogText.Log($"Store.Create: { ex.Message}");

                return View(model);
            }
        }

        [RequiredPermisson(Roles = "ViewStore")]
        public ActionResult Index(int page = 1, string keywords = "")
        {
            ViewBag.Keywords = keywords;

            var sortBuilder = new SortBuilder
            {
                ColumnName = "CreatedDate",
                ColumnOrder = SortBuilder.SortOrder.Descending
            };

            var sortingPagingBuilder = new SortingPagingBuilder
            {
                Keywords = keywords,
                Sorts = sortBuilder
            };

            var paging = new Paging
            {
                PageNumber = page,
                PageSize = CommonHelper.PageSize,
                TotalRecord = 0
            };


            var stores = _storeService.PagedList(sortingPagingBuilder, paging);
            if (stores.IsAny())
            {
                var pageInfo = new Helper.PageInfo(CommonHelper.PageSize, page, paging.TotalRecord, i => Url.Action("Index", new { page = i, keywords }));
                ViewBag.PageInfo = pageInfo;
            }

            return View(stores);
        }

        [HttpPost]
        public async Task<JsonResult> ChangeLockedOut(Guid userId, bool isLock)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false });
            }

            try
            {
                var existingUser = _userStoreService.FindByIdAsync(userId).Result;
                await _userStoreService.SetLockoutEnabledAsync(existingUser, isLock);

                return Json(
                    new
                    {
                        success = true,
                        messsage = "123"
                    },
                    JsonRequestBehavior.AllowGet
                );
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        success = false,
                        messsage = ex.Message
                    },
                    JsonRequestBehavior.AllowGet
                );
            }
        }
    }
}