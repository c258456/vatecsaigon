using App.Core.Extensions;
using App.Core.Logging;
using App.Core.Utilities;
using App.Domain.Brandes;
using App.FakeEntity.Brandes;
using App.Framework.Utilities;
using App.Service.Brandes;
using AutoMapper;
using Resources;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Admin.Helpers;

namespace App.Admin.Controllers
{
    public class BrandController : BaseAdminController
    {
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            _brandService = brandService;
        }

        [RequiredPermisson(Roles = "CreateBrand")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [RequiredPermisson(Roles = "CreateBrand")]
        public ActionResult Create(BrandViewModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var messages = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage + " " + v.Exception));

                    ModelState.AddModelError("", messages);

                    return View(model);
                }

                var modelMap = Mapper.Map<BrandViewModel, Brand>(model);
                _brandService.Create(modelMap);

                Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.CreateSuccess, FormUI.Brand)));
                return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                       returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                    ? (ActionResult) RedirectToAction("Index")
                    : Redirect(returnUrl);
            }
            catch (Exception ex)
            {
                LogText.Log($"Brand.Create: { ex.Message}");

                return View(model);
            }
        }

        [RequiredPermisson(Roles = "DeleteBrand")]
        public ActionResult Delete(string[] ids)
        {
            try
            {
                if (ids.Length != 0)
                {
                    var brands =
                        from id in ids
                        select _brandService.GetById(int.Parse(id));

                    _brandService.BatchDelete(brands);
                }
            }
            catch (Exception ex)
            {
                LogText.Log($"Brand.Delete: { ex.Message}");
            }

            return RedirectToAction("Index");
        }

        [RequiredPermisson(Roles = "ViewBrand")]
        public ActionResult Edit(int id)
        {
            var modelMap = Mapper.Map<Brand, BrandViewModel>(_brandService.GetById(id));

            return View(modelMap);
        }

        [HttpPost]
        [RequiredPermisson(Roles = "ViewBrand")]
        public ActionResult Edit(BrandViewModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var messages = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage + " " + v.Exception));

                    ModelState.AddModelError("", messages);
                    return View(model);
                }

                var modelMap = Mapper.Map<BrandViewModel, Brand>(model);
                _brandService.Update(modelMap);

                Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.UpdateSuccess, FormUI.Brand)));
                return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                       returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                    ? (ActionResult) RedirectToAction("Index")
                    : Redirect(returnUrl);
            }
            catch (Exception ex)
            {
                LogText.Log($"Brand.Create: { ex.Message}");

                return View(model);
            }
        }

        [RequiredPermisson(Roles = "ViewBrand")]
        public ActionResult Index(int page = 1, string keywords = "")
        {
            ViewBag.Keywords = keywords;
            var sortingPagingBuilder = new SortingPagingBuilder
            {
                Keywords = keywords,
                Sorts = new SortBuilder
                {
                    ColumnName = "Name",
                    ColumnOrder = SortBuilder.SortOrder.Descending
                }
            };
            var paging = new Paging
            {
                PageNumber = page,
                PageSize = PageSize,
                TotalRecord = 0
            };

            var brands = _brandService.PagedList(sortingPagingBuilder, paging);
            if (brands.IsAny())
            {
                var pageInfo = new Helper.PageInfo(CommonHelper.PageSize, page, paging.TotalRecord, i => Url.Action("Index", new { page = i, keywords }));
                ViewBag.PageInfo = pageInfo;
            }

            return View(brands);
        }
    }
}