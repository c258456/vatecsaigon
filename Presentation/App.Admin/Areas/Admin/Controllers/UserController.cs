using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using App.Domain.Entities.Identity;
using App.FakeEntity.User;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Resources;

namespace App.Admin.Controllers
{
    public class UserController : BaseDefaultIdentity
    {
        public UserController(UserManager<IdentityUser, Guid> userManager) : base(userManager)
        {
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            ActionResult actionResult;

            var flag = HasPassword();
            ViewBag.HasLocalPassword = flag;
            ViewBag.ReturnUrl = Url.Action("Index", "Home");

            if (!flag)
            {
                var item = ModelState["OldPassword"];
                if (item != null)
                {
                    item.Errors.Clear();
                }
                if (ModelState.IsValid)
                {
                    var identityResult = await UserManager.AddPasswordAsync(GetGuid(User.Identity.GetUserId()), model.NewPassword);
                    var identityResult1 = identityResult;
                    if (!identityResult1.Succeeded)
                    {
                        AddErrors(identityResult1);
                    }
                    else
                    {
                        Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.UpdateSuccess, FormUI.Password)));
                        actionResult = View();
                        return actionResult;
                    }
                }
            }
            else if (ModelState.IsValid)
            {
                var identityResult2 = await UserManager.ChangePasswordAsync(GetGuid(User.Identity.GetUserId()), model.OldPassword, model.NewPassword);
                var identityResult3 = identityResult2;
                if (!identityResult3.Succeeded)
                {
                    AddErrors(identityResult3);
                }
                else
                {
                    Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.ErrorMessageWithFormat, FormUI.Password)));
                    actionResult = View();
                    return actionResult;
                }
            }
            actionResult = View();
            return actionResult;
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel login, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var exstingUser = await UserManager.FindAsync(login.UserName, login.Password);

            if (exstingUser != null)
            {
                await SignInAsync(exstingUser, login.Remember);

                return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                       returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                    ? (ActionResult)RedirectToAction("Index", "Home")
                    : Redirect(returnUrl);
            }

            ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");

            return View();
        }

        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            return RedirectToAction("Login");
        }

        private async Task SignInAsync(IdentityUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            Session.Abandon();

            var claimsIdentity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            var authenticationManager = AuthenticationManager;

            authenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = isPersistent
            }, claimsIdentity);
        }
    }
}