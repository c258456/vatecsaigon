using App.Core.Logging;
using App.Core.Utilities;
using App.Domain.Entities.Identity;
using App.FakeEntity.User;
using App.Framework.Utilities;
using App.Service.Account;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Resources;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using App.Admin.Helpers;

namespace App.Admin.Controllers
{
    public class RoleController : BaseIdentityController
    {
        private readonly IRoleService _roleService;
        private readonly RoleManager<IdentityRole, Guid> _roleManager;

        public RoleController(IRoleService roleService,
            UserManager<IdentityUser, Guid> userManager,
            RoleManager<IdentityRole, Guid> roleManager) : base(userManager)
        {
            _roleService = roleService;
            _roleManager = roleManager;
        }

        [RequiredPermisson(Roles = "CreateRole")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [RequiredPermisson(Roles = "CreateRole")]
        public ActionResult Create(RoleManagerModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var msg = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage + " " + v.Exception));

                    ModelState.AddModelError("", msg);

                    return View(model);
                }

                var identityRole = Mapper.Map<IdentityRole>(model);
                var identityResult = _roleManager.Create(identityRole);

                if (identityResult.Succeeded)
                {
                    Response.Cookies.Add(new HttpCookie("system_message", string.Format(MessageUI.CreateSuccess, FormUI.Roles)));

                    return !Url.IsLocalUrl(returnUrl) || returnUrl.Length <= 1 || !returnUrl.StartsWith("/") ||
                           returnUrl.StartsWith("//") || returnUrl.StartsWith("/\\")
                        ? (ActionResult)RedirectToAction("Index")
                        : Redirect(returnUrl);
                }

                AddErrors(identityResult);

                return View(model);

            }
            catch (Exception ex)
            {
                LogText.Log($"Role.Create: { ex.Message}");

                return View(model);
            }
        }

        [RequiredPermisson(Roles = "ViewRole")]
        public async Task<ActionResult> Index(int page = 1, string keywords = "")
        {
            ViewBag.Keywords = keywords;
            var sortingPagingBuilder = new SortingPagingBuilder
            {
                Keywords = keywords
            };
            var sortBuilder = new SortBuilder
            {
                ColumnName = "Name",
                ColumnOrder = SortBuilder.SortOrder.Descending
            };
            sortingPagingBuilder.Sorts = sortBuilder;
            var sortingPagingBuilder1 = sortingPagingBuilder;
            var paging = new Paging
            {
                PageNumber = page,
                PageSize = PageSize,
                TotalRecord = 0
            };

            var roles = await _roleService.PagedList(sortingPagingBuilder1, paging);
            if (roles == null || !roles.Any())
            {
                return View(roles);
            }

            var pageInfo = new Helper.PageInfo(CommonHelper.PageSize, page, paging.TotalRecord, i => Url.Action("Index", new { page = i, keywords }));
            ViewBag.PageInfo = pageInfo;

            return View(roles);
        }
    }
}